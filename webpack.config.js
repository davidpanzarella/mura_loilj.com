
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

// ===========================================================================
// CONSTANTS
// ===========================================================================
const SITE_NAME = 'default';
const THEME_NAME = 'bs4-loilj';
const HOST = 'localhost';
const PORT = 3000;
const DEVELOPMENT = 'development';
const PRODUCTION = 'production';

var config;
var jsLoader;
var cssLoader;
var scssLoader;
var devServer;

// ===========================================================================
// SETUP ENV
// ===========================================================================
const TARGET = process.env.npm_lifecycle_event;
const ENV = getEnv(TARGET);
//const WATCH = global.watch || false;

// ===========================================================================
// UTILS
// ===========================================================================
function getEnv(target) {
  switch (target) {
    case 'start':
      return DEVELOPMENT;

    case 'build':
      return PRODUCTION;

    case 'stats':
      return PRODUCTION;

    default:
      return DEVELOPMENT;
  }
}

function unipath(base) {
  return function join(/* ...paths */) {
    // eslint-disable-next-line prefer-rest-params
    const _paths = [ base ].concat(Array.from(arguments));
    return path.resolve(path.join.apply(null, _paths));
  };
}

// ===========================================================================
// CONFIG ENV DEFINITIONS
// ===========================================================================
function getEntry(env) {
  const entry = {};
  entry.bundle = [
  	'./node_modules/bootstrap/dist/js/bootstrap.js',
  	'./node_modules/trmix/dist/trmix.min.js',
    './node_modules/fitvids/fitvids.min.js',
		'./node_modules/cloudinary-jquery/cloudinary-jquery.js',
    './node_modules/flexslider/jquery.flexslider-min.js',
	  PATHS.src('scripts', 'main.js'),
  ];

  switch (env) {
    case DEVELOPMENT:
      // entry.bundle.unshift('webpack/hot/only-dev-server');
      entry.bundle.unshift(`webpack-hot-middleware/client?http://${HOST}:${PORT}&reload=true`);
      // entry.bundle.push(PATHS.src('styles', 'main.scss'));
      break;

    case PRODUCTION:
      // entry.style.push(PATHS.src('styles', 'main.scss'));
      break;
  }
  return entry;
}

const nodeEnv = process.env.NODE_ENV || 'dev';
const PATHS = {
	build: unipath(`./${SITE_NAME}/includes/themes/${THEME_NAME}/dist`),
	src: unipath(`./${SITE_NAME}/includes/themes/${THEME_NAME}/src`),
};

if (ENV === DEVELOPMENT) {
  PATHS.build = unipath(`./${SITE_NAME}/includes/themes/${THEME_NAME}/dev`)
}

config = {
  devtool: 'source-map',
  output: {
    path: PATHS.build(),
    publicPath: ENV === PRODUCTION ? '/' : `/${SITE_NAME}/includes/themes/${THEME_NAME}/dist`,
    filename: '[name].js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    // new HtmlWebpackPlugin({
    //   template: 'app/index.html',
    //   inject: 'body',
    // })
    new WriteFilePlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
      }
    }),
  ],
  module: {
    loaders: [
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'url-loader?limit=8192',
          'img-loader'
        ]
      },
      {
        test: /\.modernizrrc$/,
        loader: "modernizr-loader"
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
		alias: {
      modernizr$: path.resolve(__dirname, "./.modernizrrc")
    }
  },
};

config.entry = getEntry(ENV);

console.log(`publicPath: ${config.output.publicPath}`);

jsLoader = {
  test: /\.(js|jsx)$/,
  exclude: /node_modules/,
  loaders: [
    {
      loader: 'babel-loader',
      query: {
        presets: [
          ['es2015', { 'modules': false }]
        ]
      }
    }
  ]
};

cssLoader = {
  test: /\.css$/,
  loader: 'style-loader!css-loader!postcss-loader'
};

scssLoader = {
  test: /\.scss$/,
  loaders: [
    'style-loader',
    'css-loader',
    'postcss-loader',
    'sass-loader?outputStyle=expanded'
  ]
};

// If we're running under the npm build script,
// then use the ExtractTextPlugin to generate
// a stylesheet bundle.
console.log(`npm lifecycle event: ${process.env.npm_lifecycle_event}`);
if ((process.env.npm_lifecycle_event === 'build') ||
    (process.env.npm_lifecycle_event === 'dev')   ||
    (process.env.npm_lifecycle_event === 'bs')) {
  config.plugins.push(
    new ExtractTextPlugin('bundle.css'/*, { allChunks: true }*/)
  );

  cssLoader.loader = ExtractTextPlugin.extract({
    fallbackLoader: 'style-loader',
    loader: 'css-loader'
  })

  delete scssLoader.loaders;
  scssLoader.loader = ExtractTextPlugin.extract({
    fallbackLoader: 'style-loader',
    loader: ['css-loader', 'postcss-loader', 'sass-loader?outputStyle=expanded']
  });

}

// Enable HMR if not explicitly disabled by environment variable
// if (process.env.DISABLE_HMR !== 'true') {
//   // config.entry.splice(0, 0,
//   //   'webpack/hot/dev-server',
//   //   'webpack-hot-middleware/client'
//   // );
//   config.plugins.push(
//     new webpack.HotModuleReplacementPlugin()
//   );

//   jsLoader.loaders[0].query.presets.push('react-hmre');
//   jsLoader.loaders.splice(0, 0, {
//     loader: 'react-hot-loader/webpack'
//   });

//   // Turn on HMR for webpack-dev-server
//   devServer = {
//     hot: true
//   };
//   config.devServer = devServer;

//   console.log('HMR enabled.');
// }

// Add loaders to config for CSS, SCSS, JavaScript (& JSX)
config.module.loaders.push(cssLoader);
config.module.loaders.push(scssLoader);
config.module.loaders.push(jsLoader);

module.exports = config;
