/* eslint default-case:0 */
global.watch = true;

const path = require('path');
const fs = require('fs-extra');
const browserSync = require('browser-sync').create();
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const htmlInjector = require('bs-html-injector');
const webpackConfig = require('./webpack.config');

const bundler = webpack(webpackConfig);

// ===========================================================================
// CONFIG
// ===========================================================================
const PROXY_TARGET = 'localhost:8888';

const bsOptions = {
  files: ["./**/*.css", "./**/*.js"],
  proxy: {
    // proxy local WP install
    target: PROXY_TARGET,

    middleware: [
      // converts browsersync into a webpack-dev-server
      webpackDevMiddleware(bundler, {
        publicPath: webpackConfig.output.publicPath,
        noInfo: false,
        stats: {
          colors: true,
          verbose: true,
        }
      }),

      // hot update js && css
      webpackHotMiddleware(bundler),
    ],
  },

  //this gets annoying
  open: false,
  logLevel: 'debug',
};

// setup html injector, only compare differences within outer most div (#page)
// otherwise, it would replace the webpack HMR scripts
// TOOK OUT , { restrictions: [ 'body' ] }. Lets test
browserSync.use(htmlInjector);

// register the plugin
browserSync.use(htmlInjector, {
    // Files to watch that will trigger the injection
    files: "./**/*.cfm"
});


// ===========================================================================
// RUN
// ===========================================================================
// clean -> ensure 'style.css' -> run browsersync
fs.emptyDir(webpackConfig.output.path, () => (
  browserSync.init(bsOptions)
));
