//-----------------------------------------------------------------------------------
 // Author: David Panzarella (SB InnerWeb Development - www.sbinnerweb.com)
 // Date: 2/28/2012
 // This file contains scripts for various JS functions
//-----------------------------------------------------------------------------------

// @codekit-prepend "vendor/ie10-viewport-bug-workaround.js"
// @codekit-prepend "vendor/trmix.js"
// @codekit-prepend "vendor/detectmobilebrowser.js"
// @codekit-prepend "vendor/retina-1.1.0.min.js"
// @codekit-prepend "vendor/jquery.appear.js"
// @codekit-prepend "vendor/jquery.countTo.js"
// @codekit-prepend "vendor/jquery.placeholder.min.js"
// @codekit-prepend "../assets/bootstrapvalidator/dist/js/bootstrapValidator.js"
// @codekit-prepend "../assets/flexslider/jquery.easing.js
// @codekit-prepend "../assets/flexslider/jquery.mousewheel.js
// @codekit-prepend "../assets/flexslider/jquery.flexslider.js
// @codekit-prepend "../assets/fancybox/source/jquery.fancybox.js
// @codekit-prepend "../assets/fancybox/source/helpers/jquery.fancybox-thumbs.js

/* ========================== */
/* ==== HELPER FUNCTIONS ==== */
isIE = false;
$ = jQuery.noConflict();

var isiPad = (navigator.userAgent.match(/iPad/i) !== null);

$.fn.isAfter = function (sel) {
	return this.prevAll(sel).length !== 0;
};

$.fn.isBefore = function (sel) {
	return this.nextAll(sel).length !== 0;
};

function validatedata($attr, $defaultValue) {
	if ($attr !== undefined) {
		return $attr;
	}
	return $defaultValue;
}

function parseBoolean(str, $defaultValue) {
	if (str === 'true') {
		return true;
	}
	return $defaultValue;
	//return /true/i.test(str);
}


//-----------------------------------------------------------------------------------
// iPhone Viewpoint Fix
//-----------------------------------------------------------------------------------
if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
	var viewportmeta = document.querySelector('meta[name="viewport"]');
	if (viewportmeta) {
		viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
		document.body.addEventListener('gesturestart', function () {
				viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
		}, false);
	}
}

jQuery(document).ready(function () {
	"use strict";
	$ = jQuery.noConflict();

	/* ============================= */
	/* ==== SET ELEMENTS HEIGHT ==== */
	// flexslider
	$('.flexslider.std-slider').each(function () {
		var $this = $(this);
		$this.css('min-height', $this.attr('data-height') + "px");
	});

	// spacer element
	$('.spacer').each(function () {
		var $this = $(this);
		$this.css('height', $this.attr('data-height') + "px");
	});


  /* ================================== */
  /* ==== SET PADDING FOR SECTIONS ==== */

  $(".content-area, .parallaxSection").each(function () {
    var $this = $(this);
    var bottomSpace = $this.attr("data-btmspace");
    var topSpace = $this.attr("data-topspace");
    var bg = $this.attr("data-bg");

    if (validatedata(bottomSpace, false)) {
      $this.css("padding-bottom", bottomSpace + "px");
    }
    if (validatedata(topSpace, false)) {
      $this.css("padding-top", topSpace + "px");
    }
    if (validatedata(bg, false)) {
      $this.css('background-image', 'url("' + bg + '")');
    }
  });


	if ($(".parallaxSection.height100").length > 0) {

		$(".parallaxSection.height100").each(function () {

			var $this = $(this);
			$("#boxedWrapper, body").css("height", "100%");

			var menuHeight = 0;
			if ($this.isAfter("#boxedWrapper>header .navbar-default")) {
				menuHeight = $("#boxedWrapper>header .navbar-default").outerHeight();
			}
			if ($("#boxedWrapper>header .navbar-default").hasClass("navbar-fixed-top")) {
				menuHeight = 0;
			}
			// console.log($this.outerHeight());

			var sliderHeight = $this.outerHeight() - menuHeight;
			var $slider = $this.find(".flexslider");

			$($this, $slider).css("height", sliderHeight);
			// console.log(menuHeight);
			// console.log(sliderHeight);
			// console.log($slider);
		});
	}


	/* ========================== */
	/* ==== SCROLL TO ANCHOR ==== */

	$('a.local[href*=#]:not([href=#])').click(function () {
		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

			var menuOffset = 0;
			if ($(this).hasClass("menuOffset")) {
				// menuOffset = parseInt($('.navbar-default').height());
				menuOffset = 87;
			}

			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - menuOffset
				}, 1000, 'swing');
				return false;
			}

		}
	});

  /* ======================== */
  /* ==== ANIMATION INIT ==== */

  if ($().appear) {

    if (($.browser.mobile) || (isiPad)) {
      // disable animation on mobile
      $("body").removeClass("withAnimation");
    } else {

      $('.withAnimation .animated').appear(function () {
        var $this = $(this);
        $this.each(function () {
          $this.addClass('activate');
          $this.addClass($this.data('fx'));
        });
      }, {accX: 50, accY: -150});

    }
  }

	/* ================== */
	/* ==== COUNT TO ==== */

	if (($().appear) && ($(".timerCounter").length > 0)) {
		$('.timerCounter').appear(function () {
			$('.timerVal').each(function () {
				$(this).countTo();
			});
		});
	}

	/* =============================== */
	/* ==== PLACEHOLDERS FALLBACK ==== */

	if ($().placeholder) {
		$("input[placeholder],textarea[placeholder]").placeholder();
	}

	/* ======================= */
	/* ==== TOOLTIPS INIT ==== */

	$("[data-toggle='tooltip']").tooltip();


	/* ======================= */
	/* ==== TO TOP BUTTON ==== */

	$('#toTop').click(function () {
		$("body,html").animate({scrollTop: 0}, 600);
		return false;
	});

	$(window).scroll(function () {
		if ($(this).scrollTop() !== 0) {
			$("#toTop").fadeIn(300);
		} else {
			$("#toTop").fadeOut(250);
		}
	});


	// /* ======================== */
	// /* ==== MAGNIFIC POPUP ==== */

	// if ($().magnificPopup) {

	// 	$(".popup-iframe").magnificPopup({
	// 		disableOn: 700,
	// 		type: 'iframe',
	// 		mainClass: 'mfp-fade',
	// 		removalDelay: 160,
	// 		preloader: false,
	// 		fixedContentPos: false
	// 	});

	// 	$('.imgpopup').magnificPopup({
	// 		type: 'image',
	// 		closeOnContentClick: true,
	// 		closeBtnInside: false,
	// 		fixedContentPos: false,
	// 		mainClass: 'mfp-fade', // class to remove default margin from left and right side
	// 		gallery: {
	// 			enabled: true,
	// 		},
	// 		image: {
	// 			verticalFit: true
	// 		}
	// 	});
	// }

	$('.fancybox-thumbs').fancybox({
		prevEffect : 'none',
		nextEffect : 'none',

		closeBtn  : false,
		arrows    : false,
		nextClick : true,

		helpers : {
			thumbs : {
				width  : 50,
				height : 50
			}
		}
	});


	$(window).load(function () {


		//-----------------------------------------------------------------------------------
		//	Scroll to header and resize as it scrolls.
		//-----------------------------------------------------------------------------------
		var headerHeight = $("body>header .navbar, #boxedWrapper>header .navbar").height();
		$("body>header, #boxedWrapper>header .navbar").height(headerHeight);
		$("body>header .navbar, #boxedWrapper>header .navbar").affix({
			offset: $("body>header+section, #boxedWrapper>header+section").position()
		});

		/* ==================== */
		/* ==== FLEXSLIDER ==== */

		if (($().flexslider) && ($(".flexslider").length > 0)) {

			$('.flexslider.std-slider').each(function () {
				var $this = $(this);

				// initialize
				$this.find(".slides > li").each(function () {
					var $slide_item = $(this);
					var bg = validatedata($slide_item.attr('data-bg'), false);
					if (bg) {
						$slide_item.css('background-image', 'url("' + bg + '")');
					}
					$slide_item.css('min-height', $this.attr('data-height') + "px");

					// hide slider content due to fade animation
					$slide_item.find(".inner").hide();

					$slide_item.find(".inner [data-fx]").each(function () {
						$(this).removeClass("animated");
					});
				});

				var loop = validatedata(parseBoolean($this.attr("data-loop")), false);
				var smooth = validatedata(parseBoolean($this.attr("data-smooth")), false);
				var slideshow = validatedata(parseBoolean($this.attr("data-slideshow")), false);
				var speed = validatedata(parseInt($this.attr('data-speed')), 7000);
				var animspeed = validatedata(parseInt($this.attr("data-animspeed")), 600);
				var controls = validatedata(parseBoolean($this.attr('data-controls')), false);
				var dircontrols = validatedata(parseBoolean($this.attr('data-dircontrols')), false);

				$this.flexslider({
					animation: "fade",              //String: Select your animation type, "fade" or "slide"
					animationLoop: loop,             //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
					smoothHeight: smooth,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
					slideshow: slideshow,                //Boolean: Animate slider automatically
					slideshowSpeed: speed,           //Integer: Set the speed of the slideshow cycling, in milliseconds
					animationSpeed: animspeed,            //Integer: Set the speed of animations, in milliseconds

					// Primary Controls
					controlNav: controls,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
					directionNav: dircontrols,             //Boolean: Create navigation for previous/next navigation? (true/false)

					pauseOnHover: true,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
					prevText: " ",           //String: Set the text for the "previous" directionNav item
					nextText: " ",
					useCSS: false,

					// Callback API
					start: function () {
						setTimeout(function () {
							$this.find(".slides > li.flex-active-slide .inner").each(function () {
								var $content = $(this);
								if (!isIE) {
									$content.closest(".inner").show();
								} else {
									$content.closest(".inner").fadeIn(300);
								}
							});
							$this.find(".slides > li.flex-active-slide .inner [data-fx]").each(function () {
								var $content = $(this);
								$content.addClass($content.data('fx')).show().addClass("animated activate");
							});
						}, 600);

					},
					before: function () {

						$this.find(".slides > li.flex-active-slide .inner [data-fx]").each(function () {
							var $content = $(this);
							$content.closest(".inner").fadeOut(400);
							$content.removeClass($content.data('fx')).removeClass("animated activate");
						});
					},           //Callback: function(slider) - Fires asynchronously with each slider animation
					after: function () {
						setTimeout(function () {
							$this.find(".slides > li.flex-active-slide .inner").each(function () {
								var $content = $(this);
								if (!isIE) {
									$content.closest(".inner").show();
								} else {
									$content.closest(".inner").fadeIn(300);
								}
							});
							$this.find(".slides > li.flex-active-slide .inner [data-fx]").each(function () {
								var $content = $(this);
								$content.addClass($content.data('fx')).show().addClass("animated activate");
							});
						}, 150);
					},            //Callback: function(slider) - Fires after each slider animation completes
					end: function () {
					},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
					added: function () {
					},            //{NEW} Callback: function(slider) - Fires after a slide is added
					removed: function () {
					}           //{NEW} Callback: function(slider) - Fires after a slide is removed
				});
			});

			$('.flexslider.carousel-slider').each(function () {
				var $this = $(this);

				var slideshow = validatedata(parseBoolean($this.attr("data-slideshow")), false);
				var speed = validatedata(parseInt($this.attr('data-speed')), 7000);
				var animspeed = validatedata(parseInt($this.attr("data-animspeed")), 600);
				var loop = validatedata(parseBoolean($this.attr("data-loop")), false);
				var min = validatedata(parseInt($this.attr('data-min')), 1);
				var max = validatedata(parseInt($this.attr("data-max")), 3);
				var move = validatedata(parseInt($this.attr("data-move")), 0);
				var controls = validatedata(parseBoolean($this.attr('data-controls')), false);
				var dircontrols = validatedata(parseBoolean($this.attr('data-dircontrols')), false);

				$this.flexslider({
					animation: "slide",
					slideshow: slideshow,
					slideshowSpeed: speed,
					animationSpeed: animspeed,
					animationLoop: loop,
					itemWidth: 370,
					itemMargin: 30,
					minItems: min,
					maxItems: max,
					move: move,
					controlNav: controls,
					directionNav: dircontrols
				});
			});
		}


		/* =========================== */
		/* === BIG ARROW ANIMATION === */

		function animateArrow() {

			setTimeout(function () {
				$(".bigArrow i").css('opacity', 1).stop(true, true).animate({ opacity: 0, top: "15px" }, { queue: false, duration: 350, complete: function () {

					$(".bigArrow i").css("top", "-15px").stop(true, true).delay(200).animate({ opacity: 1, top: 0 }, { queue: false, duration: 450, complete: function () {
						animateArrow();
					}});
				}});
			}, 1800);
		}

		animateArrow();


		// $('.navbar').on('click', function() {
		//   var height = ($('.navbar-collapse.in').length === 0) ? $(this).height() + headerHeight : 0;
		//   $('body').css({
	  //      marginTop: height
		//   });
		// });



		//-----------------------------------------------------------------------------------
		//	Initalize Fancy Box
		//-----------------------------------------------------------------------------------
		// $('.image-link').magnificPopup({type:'image'});


		//-----------------------------------------------------------------------------------
		//	Even Height - set any attributes with the .box class to autoheight for the tallest one
		//-----------------------------------------------------------------------------------
		var boxes = $('.box, .pull-height'),
				maxHeight = Math.max.apply(
					Math, boxes.map(function() {
						return $(this).height();
				}).get());
		boxes.height(maxHeight);


		//-----------------------------------------------------------------------------------
		//	Handle external links (new window)
		//-----------------------------------------------------------------------------------
		$('a[href^=http],a[class^=external]').bind('click',function(){
			window.open($(this).attr('href'));
			return false;
		});



		//-----------------------------------------------------------------------------------
		//	Jump to the Top - custom, no URL
		//-----------------------------------------------------------------------------------
		$(function() {
			$(window).scroll(function() {
				if($(this).scrollTop() !== 0) {
					$('#toTop,#bookBtn').fadeIn();
				} else {
					$('#toTop,#bookBtn').fadeOut();
				}
			});

			$('#toTop').click(function() {
				$('body,html').animate({scrollTop:0},200);
			});
		});




	});
	/* / window load */


});
/* / document ready */




