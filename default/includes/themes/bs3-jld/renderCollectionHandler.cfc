<cfcomponent
	extends     = "mura.content.contentRenderer"
	output      = "false"
	hint        = "This acts like a folder or feed, but adds in layout, searching, filtering, sorting, and messaging"
	displayname = "Collection Render Component" >

	<!--- start: dspRenderCollection --->
	<cffunction name = "dspRenderCollection"
		output      = "true"
		hint        = "This will be the primary function for accessing. Many of the variables are pay it forward variables to other functions such as dspRenderTemplate() or dspRenderFilters for other object variables"
		displayname = "Render Collection" >

		<!--- start: data, filter and sort args --->
		<cfargument name="data" type="any" hint="Accepts either be empty or feed object">
		<cfargument name="type" type="string" hint="Accepts content type (Page, Folder, etc.)">
		<cfargument name="subtype" type="string" hint="Accepts sub content type (Default, Folder, etc.)">
		<cfargument name="parentID" type="UUID" hint="Accepts a ParentID to act as a filter source">
		<cfargument name="filename" type="string" hint="Accepts a filename path, if searching a section">
		<cfargument name="contentIDList" type="string" hint="Allows the ability to pass a list of content IDs to filter results by">
		<cfargument name="maxItems" type="numeric" hint="Max number of results">
		<cfargument name="sortBy" type="string" hint="Field to sort by">
		<cfargument name="sortDirection" type="string" hint="Direction to sort">

		<!--- search term args --->
		<cfargument name="searchRelatedFields" type="string" hint="Accepts comma-delimited list of related content fields to search on">
		<cfargument name="searchCategoryFields" type="string" hint="Accepts comma-delimited list of category fields or extended attributes that contain category IDs as option lists to search on.">
		<cfargument name="searchFields" type="string" hint="Accepts comma-delimited list of search fields to search on. These should only be normal fields and extended attributes, not related sets or categorys. If we need categories or releated content, simply pass in categories or related as fields to trigger those searches.">
		<cfargument name="searchTerms" type="string" hint="Accepts comma-delimited list of search terms to be searched on">
		<cfargument name="searchOperator" type="string"hint="Accepts contains, equals, does not contain, or not equal as operators for search terms">

		<!--- filter args --->
		<cfargument name="filterFields" type="string" hint="Accepts comma-delimited list of fields to be used as search filters">
		<cfargument name="filterRelatedFields" type="string" hint="Accepts comma-delimited list of related content fields to be used as search filters">
		<cfargument name="filters" type="string" hint="Accepts whatever was passed into $.event('filters')">
		<!--- end: data, filter and sort args --->

		<!--- start: pagination args --->
		<cfargument name="nextN" type="numeric" default="12" hint="Records per page">
		<cfargument name="startRow" type="numeric" default="1" hint="Starting Row">
		<!--- end: pagination args --->

		<!--- start: layout variables --->
		<cfargument name="order" type="string" default="search,filters,message,template,message,pagination" hint="Accepts a comma-delimited list for the order of widgets are to be shown">

		<!--- search layout args --->
		<cfargument name="searchWrapper" type="string" hint="Wrapper for the search component">
		<cfargument name="searchWrapperAttributes" type="string" hint="Attributes for wrapper for search component">
		<cfargument name="searchFormAttributes" type="string" hint="Attributes for search form.">
		<cfargument name="searchInputAttributes" type="string" hint="Attributes for input on the search form.">
		<cfargument name="searchButtonAttributes" type="string" hint="Attributes for button on the search form.">
		<cfargument name="searchSuggestFields" type="string" hint="Accepts comma-delimited list of suggeted fields that will become the look up fields in the search field">

		<!--- filter layout args --->
		<cfargument name="filterView" type="boolean" default="false" hint="Show or don't show upon initial load. Will wait for search terms to be entered.">
		<cfargument name="filterMappedCategory" type="string" hint="Accepts comma-delimited list of which category the filterFields are mapped against">
		<cfargument name="filterDisplay" type="string" hint="What sort of filter you want to show (link, select, dependentSelect)">
		<cfargument name="filterType" type="string" hint="How many items you can choose per filter (single, multiple)">
		<cfargument name="filterGroup" type="string" hint="Do you want the filter in one group of split up into a group per mapped category passed in">
		<cfargument name="filterCounts" type="boolean" default="false" hint="Include counts in filters?">

		<cfargument name="filterWrapper" type="string" hint="Wrapper for filter component">
		<cfargument name="filterWrapperAttributes" type="string" hint="Attributes for wrapper for filter component">

		<cfargument name="filterContainer" type="string" hint="Container for filter component, if necessary">
		<cfargument name="filterContainerAttributes" type="string" hint="Attributes for container for filter component">

		<cfargument name="filterMessageWrapper" type="string" hint="Will be wrapped around the filter message, if necessary">
		<cfargument name="filterMessageWrapperAttributes" type="string" hint="Attributes for the filter message, if necessary">
		<cfargument name="filterMessage" type="string" hint="Message to display next to filters">
		<cfargument name="filterShowMessage" type="boolean" hint="Show the message to display next to filters">

		<cfargument name="filterGroupWrapper" type="string" hint="Wrapper to group all the filters, if necessary">
		<cfargument name="filterGroupWrapperAttributes" type="string" hint="Attributes for the group container, if necessary">
		<cfargument name="filterGroupContainer" type="string" hint="Acts as a container for the filter group, if necessary">
		<cfargument name="filterGroupContainerAttributes" type="string" hint="Attributes for the group container, if necessary">
		<cfargument name="filterGroupInnerContainer" type="string" hint="Inner container for filter component, if necessary">
		<cfargument name="filterGroupInnerContainerAttributes" type="string" hint="Attributes for inner container for filter component, if necessary">

		<cfargument name="filterAttributes" type="string" hint="List of attributes to be included in the filter wrapper">

		<!--- message layout args --->
		<cfargument name="messageWrapper" type="string" hint="Takes in a tag for the element wrapping the message, if any.">
		<cfargument name="messageWrapperAttributes" type="string" hint="Attributes for message wrapper. ID, Classes, etc.">
		<cfargument name="messageSingularType" type="string" hint="Accepts a string to tell the user if the results of specific to a certain singular type (ex: recipes)">
		<cfargument name="messagePluralType" type="string" hint="Accepts a string to tell the user if the results of specific to a certain plural type (ex: recipes)">
		<cfargument name="message" type="string" hint="Accepts a text string, which should include the variables for the counts and content types of what is being filtered or searched">

		<!--- collection (template results) layout args --->
		<cfargument name="preCollectionWrapperHR" type="string" default="false" hint="Put an HR before the collection wrapper">
		<cfargument name="collectionWrapper" type="string" hint="Takes in a tag for the element wrapping the collection, if any.">
		<cfargument name="collectionWrapperAttributes" type="string" hint="Attributes for wrapper. ID, Classes, etc." >
		<cfargument name="collectionContainer" type="string" hint="Takes in a tag for the element container, if any." >
		<cfargument name="collectionContainerAttributes" type="string" hint="Attributes for container. ID, Classes, etc." >
		<cfargument name="heading" type="string" hint="Pass in the entire html block for the heading.">
		<cfargument name="headingDesc" type="string" hint="Pass in the entire html block for the heading description.">
		<cfargument name="collectionBtnLink" type="string" hint="Pass in the link for the button">
		<cfargument name="collectionBtnLabel" type="string" hint="Pass in the label for the button">
		<cfargument name="collectionBtnAttributes" type="string" hint="Pass in the attributes for the button">
		<cfargument name="collectionInnerContainer" type="string" hint="If we need a row or something like a ul for the content within the container, set it here" >
		<cfargument name="collectionInnerContainerAttributes" type="string" hint="Attributes for the inner container. ID, Classes, etc." >
		<cfargument name="template" type="string"required="true" hint="Provides a contextual template for the collection. Ex: collection of person vs news templates">
		<cfargument name="fieldlist" type="string" hint="Limits what is displayed based on what's passed in. Obviously this depends on template included.">
		<cfargument name="templateWrapper" type="string" hint="Takes in a tag for the element wrapping the collection.">
		<cfargument name="templateWrapperAttributes" type="string" hint="Attributes for the template wrapper">
		<cfargument name="imageOrientation" type="string" hint="Refers how the image will flow around the content: left, right or top.">
		<cfargument name="imageAttributes" type="string" hint="Attributes for the image. How the image will get sized, and any other attributes needed such as width and height.">
		<cfargument name="imageSize" type="string" hint="Size for image. Should match what's in the site settings.">

		<!--- pagination layout args --->
		<cfargument name="paginationWrapper" type="string" hint="Takes in a tag for the element wrapping the paginator, if any.">
		<cfargument name="paginationWrapperAttributes" type="string" hint="Attributes for pagination wrapper. ID, Classes, etc.">

		<cfargument name="paginationContainer" type="string" hint="Container for pagination component, if necessary">
		<cfargument name="paginationContainerAttributes" type="string" hint="Attributes for container for pagination component">

		<cfargument name="paginationMessageWrapper" type="string" hint="Will be wrapped around the pagination message, if necessary">
		<cfargument name="paginationMessageWrapperAttributes" type="string" hint="Attributes for the pagination message, if necessary">
		<cfargument name="paginationShowMessage" type="boolean" hint="Show the message to display next to pagination">
		<cfargument name="paginationMessage" type="string" hint="Message to display next to pagination">

		<cfargument name="paginationInnerContainer" type="string" hint="Inner Container for pagination component, if necessary">
		<cfargument name="paginationInnerContainerAttributes" type="string" hint="Attributes for inner container for pagination component">

		<cfargument name="paginationAttributes" type="string" hint="Attributes for inner container for pagination component">

		<!--- end: layout variables --->

		<!--- start: data manipulations --->
		<cfset local.data = getData(
			data                    = arguments.data
			, type                  = arguments.type
			, subType               = arguments.subType
			, parentID              = arguments.parentID
			, filename              = arguments.filename
			, contentIDList         = arguments.contentIDList
			, maxItems              = arguments.maxItems
			, sortBy                = arguments.sortBy
			, sortDirection         = arguments.sortDirection

			, searchRelatedFields   = arguments.searchRelatedFields
			, searchCategoryFields  = arguments.searchCategoryFields
			, searchFields          = arguments.searchFields
			, searchTerms           = arguments.searchTerms
			, searchOperator        = arguments.searchOperator

			, filterRelatedFields   = arguments.filterRelatedFields
			, filterFields          = arguments.filterFields
			, filters               = arguments.filters
		)>
		<!--- end: data manipulations --->


		<!--- get the iterator from the local.data returned --->
		<cfset local.it = local.data.finalFeed.getIterator()>
		<cfset local.recordcount = local.it.getRecordCount()>

		<!--- start: pagination manipulations --->
		<cfset local.it.setNextN( arguments.nextN )>
		<cfset local.it.setStartRow( arguments.startRow )>
		<!--- end: pagination manipulations --->

		<!--- start: rendering --->
		<cfsavecontent variable="ret">
			<cfoutput>

				<!--- rendering is dependent on the order and existence of the requested elements passed into the order arg  --->
				<cfloop list="#arguments.order#" index="orderIndex">

					<cfswitch expression="#orderIndex#">

						<!--- start: search layout --->
						<cfcase value="search">
							#getLayoutSearch(
								searchWrapper             = arguments.searchWrapper
								, searchWrapperAttributes = arguments.searchWrapperAttributes
								, searchFormAttributes    = arguments.searchFormAttributes
								, searchInputAttributes   = arguments.searchInputAttributes
								, searchButtonAttributes  = arguments.searchButtonAttributes
								, searchSuggestFields     = arguments.searchSuggestFields
								, searchTerms             = arguments.searchTerms
								, filters                 = arguments.filters
							)#
						</cfcase>
						<!--- end: search layout --->

						<!--- start: filter layout --->

						<cfcase value="filter">
							#getLayoutFilter(
								data                                  = local.data
								, searchterms                         = arguments.searchterms
								, filterFields                        = arguments.filterFields
								, filters                             = arguments.filters

								, filterView                          = arguments.filterView
								, filterMappedCategory                = arguments.filterMappedCategory
								, filterDisplay                       = arguments.filterDisplay
								, filterType                          = arguments.filterType
								, filterGroup                         = arguments.filterGroup
								, filterCounts                        = arguments.filterCounts

								, filterWrapper                       = arguments.filterWrapper
								, filterWrapperAttributes             = arguments.filterWrapperAttributes
								, filterContainer                     = arguments.filterContainer
								, filterContainerAttributes           = arguments.filterContainerAttributes
								, filterWrapperAttributes             = arguments.filterWrapperAttributes
								, filterMessageWrapper                = arguments.filterMessageWrapper
								, filterMessageWrapperAttributes      = arguments.filterMessageWrapperAttributes
								, filterShowMessage                   = arguments.filterShowMessage
								, filterMessage                       = arguments.filterMessage
								, filterGroupWrapper                  = arguments.filterGroupWrapper
								, filterGroupWrapperAttributes        = arguments.filterGroupWrapperAttributes
								, filterGroupContainer                = arguments.filterGroupContainer
								, filterGroupContainerAttributes      = arguments.filterGroupContainerAttributes
								, filterGroupInnerContainer           = arguments.filterGroupInnerContainer
								, filterGroupInnerContainerAttributes = arguments.filterGroupInnerContainerAttributes
								, filterAttributes                    = arguments.filterAttributes
							)#

						</cfcase>

						<!--- end: filter layout --->

						<!--- start: message layout --->
						<cfcase value="message">
							<cfif len(arguments.searchTerms)>
								#getLayoutMessage(
									messageWrapper             = arguments.messageWrapper
									, messageWrapperAttributes = arguments.messageWrapperAttributes
									, messageCount             = local.recordcount
									, messageTerms             = arguments.searchTerms
									, messageSingularType      = arguments.messageSingularType
									, messagePluralType        = arguments.messagePluralType
									, message                  = arguments.message
								)#
							</cfif>
						</cfcase>
						<!--- end: message layout --->

						<!--- start: template layout --->
						<cfcase value="template">
							#getLayoutTemplate(
								iterator                             = local.it

								, preCollectionWrapperHR             = arguments.preCollectionWrapperHR
								, collectionWrapper                  = arguments.collectionWrapper
								, collectionWrapperAttributes        = arguments.collectionWrapperAttributes
								, collectionContainer                = arguments.collectionContainer
								, collectionContainerAttributes      = arguments.collectionContainerAttributes
								, heading                            = arguments.heading
								, headingDesc                        = arguments.headingDesc
								, collectionBtnLink                  = arguments.collectionBtnLink
								, collectionBtnLabel                 = arguments.collectionBtnLabel
								, collectionBtnAttributes            = arguments.collectionBtnAttributes

								, collectionInnerContainer           = arguments.collectionInnerContainer
								, collectionInnerContainerAttributes = arguments.collectionInnerContainerAttributes
								, template                           = arguments.template
								, fieldlist                          = arguments.fieldlist
								, templateWrapper                    = arguments.templateWrapper
								, templateWrapperAttributes          = arguments.templateWrapperAttributes
								, imageOrientation                   = arguments.imageOrientation
								, imageAttributes                    = arguments.imageAttributes
								, imageSize                          = arguments.imageSize
							)#
						</cfcase>
						<!--- end: template layout --->

						<!--- start: pagination layout --->
						<cfcase value="pagination">
							#getLayoutPagination(
								data                                 = local.data
								, nextN                              = arguments.nextN
								, startRow                           = arguments.startRow

								, paginationWrapper                  = arguments.paginationWrapper
								, paginationWrapperAttributes        = arguments.paginationWrapperAttributes
								, paginationContainer                = arguments.paginationContainer
								, paginationContainerAttributes      = arguments.paginationContainerAttributes
								, paginationMessageWrapper           = arguments.paginationMessageWrapper
								, paginationMessageWrapperAttributes = arguments.paginationMessageWrapperAttributes
								, paginationShowMessage              = arguments.paginationShowMessage
								, paginationMessage                  = arguments.paginationMessage
								, paginationInnerContainer           = arguments.paginationInnerContainer
								, paginationInnerContainerAttributes = arguments.paginationInnerContainerAttributes
								, paginationAttributes               = arguments.paginationAttributes
							)#
						</cfcase>
						<!--- end: pagination layout --->

					</cfswitch>
				</cfloop>

			</cfoutput>
		</cfsavecontent>
		<!--- end: rendering --->

		<cfreturn ret>
	</cffunction>
	<!--- end: dspRenderCollection --->

	<!---
	*
	* start: layout functions
	*
	--->

	<!--- start: search layout --->
	<cffunction name="getLayoutSearch" access="public" returntype="string" output="false">

		<cfargument name="searchWrapper" type="string" default="div"  hint="Wrapper for the search component">
		<cfargument name="searchWrapperAttributes" type="string" hint="Attributes for wrapper for search component">
		<cfargument name="searchFormAttributes" type="string" hint="Attributes for search form.">
		<cfargument name="searchInputAttributes" type="string" default='class="form-control" placeholder="Enter your keywords here"' hint="Attributes for input on the search form.">
		<cfargument name="searchButtonAttributes" type="string" default='class="btn btn-primary"' hint="Attributes for button on the search form.">
		<cfargument name="searchSuggestFields" type="string" hint="Accepts comma-delimited list of suggeted fields that will become the look up fields in the search field">
		<cfargument name="searchTerms" type="string" hint="Accepts list of search terms.">
		<cfargument name="filters" type="string" hint="">

		<!--- start: form attributes check --->
		<cfif len(arguments.searchFormAttributes)>
			<cfset local.searchFormAttributes = arguments.searchFormAttributes />
		<cfelse>
			<cfset local.searchFormAttributes = 'name="search" id="searchForm"' />
		</cfif>
		<!--- end: form attributes check --->

		<!--- start: search layout --->
		<cfoutput>
			<cfsavecontent variable="ret">

				<#arguments.searchWrapper# #arguments.searchWrapperAttributes#>
					<form role="form" action="" method="get" #local.searchFormAttributes#>
						<div class="input-group">
							<input type="text" name="keywords" id="keywords" value="#arguments.searchTerms#" #arguments.searchInputAttributes#>
							<span class="input-group-btn">
								<button type="submit" name="btnSearch" id="btnSearch" #arguments.searchButtonAttributes#><i class="fa fa-search"></i></button>
							</span>
						</div>
						<!--- adds the current filters as hidden form vars so if we change our search term, the filters stay put --->
						<cfset local.cFilters = _parseFilters( arguments.filters )>
						<cfloop array="#local.cFilters#" index="filterIndex">
							<input type="hidden" name="#filterIndex.key#" value="#filterIndex.value#">
						</cfloop>

					</form>
				</#arguments.searchWrapper#>

			</cfsavecontent>
		</cfoutput>
		<!--- end: search layout --->

		<cfreturn ret>
	</cffunction>
	<!--- end: search layout --->

	<!--- start: message layout --->
	<cffunction name="getLayoutMessage" access="public" returntype="string" output="false">
		<cfargument name="messageWrapper" default="div" type="string" hint="Takes in a tag for the element wrapping the message, if any.">
		<cfargument name="messageWrapperAttributes" type="string" default='class="alert alert-info mdm"' hint="Attributes for message wrapper. ID, Classes, etc.">
		<cfargument name="messageCount" type="numeric" hint="Accepts a number to tell us how many results we have">
		<cfargument name="messageTerms" type="string" hint="Accepts a text string, which should include the terms that were searched on">
		<cfargument name="messageSingularType" type="string" hint="Accepts a string to tell the user if the results of specific to a certain singular type (ex: recipes)">
		<cfargument name="messagePluralType" type="string" hint="Accepts a string to tell the user if the results of specific to a certain plural type (ex: recipes)">
		<cfargument name="message" type="string" hint="Accepts a text string, which should include the variables for the counts and content types of what is being filtered or searched">

		<!--- start: message type --->
		<!--- labels for search results, so that we know what to show, and whether its singular or plural --->
		<cfif len(arguments.messageSingularType) AND len(arguments.messagePluralType)>
			<cfif arguments.messageCount gt 1 OR  arguments.messageCount eq 0>
				<cfset local.messageType = arguments.messagePluralType />
			<cfelse>
				<cfset local.messageType = arguments.messageSingularType />
			</cfif>
		<cfelse>
			<cfif arguments.messageCount gt 1>
				<cfset local.messageType = 'results' />
			<cfelse>
				<cfset local.messageType = 'result' />
			</cfif>
		</cfif>
		<!--- end: message type --->

		<!--- start: message check --->
		<cfif len(arguments.message)>
			<cfset local.message = arguments.message />
		<cfelse>
			<cfset local.message = 'Found <strong>' & #arguments.messageCount# & '</strong> ' & #local.messageType# & ' searching with ' />
		</cfif>
		<!--- end: message check --->

		<!--- start: search layout --->
		<cfoutput>
			<cfsavecontent variable="ret">

				<#arguments.messageWrapper# #arguments.messageWrapperAttributes#>
					#local.message#
					<cfloop list="#arguments.messageTerms#" index="i" delimiters=", ">
						#i#<cfif i lt listLen(arguments.messageTerms)>, </cfif>
					</cfloop>
				</#arguments.messageWrapper#>

			</cfsavecontent>
		</cfoutput>
		<!--- end: search layout --->

		<cfreturn ret>
	</cffunction>
	<!--- end: message layout --->

	<!--- start: filter layout --->
	<cffunction name="getLayoutFilter" access="public" returntype="string" output="false">

		<cfargument name="data" type="any" hint="Should be iterator of already searched and filtered results.">
		<cfargument name="searchTerms" type="string" default="" hint="passed in from dspRenderCollection">

		<cfargument name="filterFields" type="string" hint="Accepts comma-delimited list of fields to be used as search filters">
		<cfargument name="filterRelatedFields" type="string" hint="Accepts comma-delimited list of related content fields to be used as search filters">
		<cfargument name="filters" type="string" default="" hint="Accepts whatever was passed into $.event('filters')">

		<cfargument name="filterView" type="boolean" default="false" hint="Show or don't show upon initial load. Will wait for search terms to be entered.">
		<cfargument name="filterMappedCategory" type="string" hint="Accepts comma-delimited list of which category the filterFields are mapped against">
		<cfargument name="filterDisplay" type="string" hint="What sort of filter you want to show (link, select, dependentSelect)">
		<cfargument name="filterType" type="string" hint="How many items you can choose per filter (single, multiple)">
		<cfargument name="filterGroup" type="string" hint="Do you want the filter in one group of split up into a group per mapped category passed in">
		<cfargument name="filterCounts" type="boolean" default="false" hint="Include counts in filters?">

		<cfargument name="filterWrapper" type="string" default="div" hint="Wrapper for filter component">
		<cfargument name="filterWrapperAttributes" type="string" default='class="row"' hint="Attributes for wrapper for filter component">

		<cfargument name="filterContainer" type="string" default="" hint="Container for filter component, if necessary">
		<cfargument name="filterContainerAttributes" type="string" default="" hint="Attributes for container for filter component">

		<cfargument name="filterMessageWrapper" type="string" default="h4" hint="Will be wrapped around the filter message, if necessary">
		<cfargument name="filterMessageWrapperAttributes" type="string" default='class="filter-message col-md-2"' hint="Attributes for the filter message, if necessary">
		<cfargument name="filterShowMessage" type="boolean" default="true" hint="Show the message to display next to filters">
		<cfargument name="filterMessage" type="string" default="Filter by:" hint="Message to display next to filters">

		<cfargument name="filterGroupWrapper" type="string" default="nav" hint="Wrapper to group all the filters, if necessary">
		<cfargument name="filterGroupWrapperAttributes" type="string" default='class="filter-nav col-md-10"' hint="Attributes for the group container, if necessary">
		<cfargument name="filterGroupContainer" type="string" default="" hint="Acts as a container for the filter group, if necessary">
		<cfargument name="filterGroupContainerAttributes" type="string" default="" hint="Attributes for the group container, if necessary">
		<cfargument name="filterGroupInnerContainer" type="string" default="" hint="Inner container for filter component, if necessary">
		<cfargument name="filterGroupInnerContainerAttributes" type="string" default="" hint="Attributes for inner container for filter component, if necessary">

		<cfargument name="filterAttributes" type="string" default='class="nav nav-pills"'hint="List of attributes to be included in the filter wrapper">


		<cfset local.ret = "">
		<cfset local.uuid = createuuid()>

		<cfset local.searchQuery = arguments.data.searchedFeedQuery>
		<cfset local.finalQuery = arguments.data.finalFeed.getQuery()>

		<cfset local.searchContentIdList = ValueList( local.searchQuery.contentId )>
		<cfset local.finalContentIdList = ValueList( local.finalQuery.contentId )>
		<cfset local.contentIdList = local.finalContentIdList>

		<cfset local.filters = _parseFilters( arguments.filters )>

		<cfset local.recordCount = 4>

		<!--- start: message check --->
		<cfif len(arguments.filterMessage)>
			<cfset local.filterMessage = arguments.filterMessage />
		<cfelse>
			<cfset local.filterMessage = 'Found <strong>' & #arguments.messageCount# & '</strong> ' & #local.messageType# & ' searching with ' />
		</cfif>
		<!--- end: message check --->


		<!--- start: filters --->
		<cfif (arguments.filterView eq true OR len( arguments.searchTerms )) AND local.recordCount gt 0>

			<!--- start: rendered filter component --->
			<cfsavecontent variable="local.ret">
				<cfoutput>

					<!--- start: filter wrapper --->
					<#arguments.filterWrapper# #arguments.filterWrapperAttributes#>

						<!--- start: filter container --->
						<cfif len(arguments.filterContainer)><#arguments.filterContainer# #arguments.filterContainerAttributes#></cfif>

							<!--- start: filter message --->
							<cfif arguments.filterShowMessage>
								<#arguments.filterMessageWrapper# #arguments.filterMessageWrapperAttributes#>#local.filterMessage#</#arguments.filterMessageWrapper#>
							</cfif>
							<!--- end: filter message --->

							<!--- start: filter group wrapper --->
							<#arguments.filterGroupWrapper# #arguments.filterGroupWrapperAttributes#>
								<cfif len(arguments.filterGroupContainer)><#arguments.filterGroupContainer# #arguments.filterGroupContainerAttributes#></cfif>
									<form action="" name="#local.uuid#" id="#local.uuid#" method="get">
                    <input type="hidden" name="keywords" id="keywords" value="#arguments.searchTerms#">
										<!--- start: display filter - link, select or dependent select --->
										<cfswitch expression="#arguments.filterDisplay#">

											<!--- start: link based filter --->
											<cfcase value="link">

												<!--- start: check for single or multiple filter --->
												<cfswitch expression="#arguments.filterGroup#">

													<!--- start: single link filter --->
													<cfcase value="single">
														<ul #arguments.filterAttributes#>
															<li <cfif arguments.filters eq "filter=">class="active"</cfif>><a href="?filter=&keywords=#arguments.searchTerms#" title="Remove all filters">Show All</a></li>
															<cfloop from="1" to="#listlen( arguments.filterFields )#" index="count">

																<cfset getKids = getFilterItems( listGetAt( arguments.filterMappedCategory , count ), listGetAt( arguments.filterFields, count ), local.contentIdList )>

																<cfloop array="#getKids#" index="kid">
																	<cfset local.thisFilterName = local.filters[1].key>
																	<cfset local.filterValue = local.filters[1].value>

																	<cfif listFind( local.filterValue, kid.catId, ':' )>
																		<li class="active"><a href="?#local.thisFilterName#=#_buildExistingFilters( local.thisFilterName, kid.catId, local.filters, arguments.filterType, arguments.searchTerms )#" title="Remove #kid.name# Filter">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></a></li>
																	<cfelse>
																		<cfif kid.count gt 0>
																			<li><a href="?#local.thisFilterName#=#_buildExistingFilters( local.thisFilterName, kid.catId, local.filters, arguments.filterType, arguments.searchTerms )#" title="Filter by #kid.name#">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></a></li>
																		</cfif>
																	</cfif>
																</cfloop>

															</cfloop>
														</ul>
													</cfcase>
													<!--- end: single link filter --->

													<!--- start: multiple link filter --->
													<cfcase value="multiple">
														<cfset getKids = getFilterItems( listGetAt( arguments.filterMappedCategory , count ), listGetAt( arguments.filterFields, count ), local.contentIdList )>
														<cfif len(arguments.filterGroupInnerContainer)><#arguments.filterGroupInnerContainer# #arguments.filterGroupInnerContainerAttributes#></cfif>
															<!--- <h2>#listgetat( arguments.filterMappedCategory, count )#</h2> --->
															<ul #arguments.filterAttributes#>
																<cfloop array="#getKids#" index="kid">
																	<cfset local.thisFilterName = local.filters[count].key>
																	<cfset local.filterValue = local.filters[count].value>
																	<cfif listFind( local.filterValue, kid.catId, ':' )>
																		<li><a href="?#local.thisFilterName#=#_buildExistingFilters( local.thisFilterName, kid.catId, local.filters, arguments.filterType )#" title="Remove #kid.name# Filter" class="active">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif> : found</a></li>
																	<cfelse>
																		<li><a href="?#local.thisFilterName#=#_buildExistingFilters( local.thisFilterName, kid.catId, local.filters, arguments.filterType )#" title="Filter by #kid.name#">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif> : not found</a></li>
																	</cfif>
																</cfloop>
															</ul>
														<cfif len(arguments.filterGroupInnerContainer)></#arguments.filterGroupInnerContainer#></cfif>
													</cfcase>
													<!--- end: multiple link filter --->

												</cfswitch>
												<!--- end: check for single or multiple filter --->

												<!--- start: TODO: related fields --->

												<!--- end: related fields --->

											</cfcase>
											<!--- end: link based filter --->

											<!--- start: select based filter --->
											<cfcase value="select">

												<cfswitch expression="#arguments.filterGroup#">

													<cfcase value="single">
														<!--- <span #arguments.filterMessageAttributes#>#arguments.filterMessage#</span> --->
														<select name="#local.filters[1].key#" id="filters-#local.filters[1].key#" #arguments.filterAttributes# onChange="this.form.submit();" <cfif arguments.filterType eq 'multiple'>multiple</cfif>>
															<cfloop from="1" to="#listlen( arguments.filterFields )#" index="count">

																<cfset getKids = getFilterItems( listGetAt( arguments.filterMappedCategory , count ), listGetAt( arguments.filterFields, count ), local.contentIdList )>
																<option value="">Select #listGetAt( arguments.filterMappedCategory , count)#</option>
																<cfloop array="#getKids#" index="kid">

																	<cfset local.thisFilterName = local.filters[1].key>
																	<cfset local.filterValue = local.filters[1].value>

																	<cfif listFind( local.filterValue, kid.catId )>
																		<option value="#kid.catId#" selected="selected">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></option>
																	<cfelse>
																		<option value="#kid.catId#">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></option>
																	</cfif>
																</cfloop>
															</cfloop>
														</select>
													</cfcase>

													<cfcase value="multiple">
														<cfloop from="1" to="#listlen( arguments.filterFields )#" index="count">

															<!--- <h2>#listgetat( arguments.filterMappedCategory, count )#</h2> --->
															<cfif len(arguments.filterGroupInnerContainer)><#arguments.filterGroupInnerContainer# #arguments.filterGroupInnerContainerAttributes#></cfif>
																<!--- <span #arguments.filterMessageAttributes#>#arguments.filterMessage#</span> --->
																<select name="#local.filters[count].key#" id="filters-#local.filters[count].key#" #arguments.filterAttributes# onChange="this.form.submit();" <cfif arguments.filterType eq 'multiple'>multiple</cfif>>
																	<cfset getKids = getFilterItems( listGetAt( arguments.filterMappedCategory , count ), listGetAt( arguments.filterFields, count ), local.contentIdList )>
																	<option value="">Select #listGetAt( arguments.filterMappedCategory , count)#</otpion>
																	<cfloop array="#getKids#" index="kid">
																		<cfset local.thisFilterName = local.filters[count].key>
																		<cfset local.filterValue = local.filters[count].value>

																		<cfif listFind( local.filterValue, kid.catId )>
																			<option value="#kid.catId#" selected="selected">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></option>
																		<cfelse>
																			<option value="#kid.catId#">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></option>
																		</cfif>
																	</cfloop>
																</select>
															<cfif len(arguments.filterGroupInnerContainer)></#arguments.filterGroupInnerContainer#></cfif>
														</cfloop>
													</cfcase>

												</cfswitch>

											</cfcase>
											<!--- end: select based filter --->

											<!--- start: dependent select based filter --->
											<cfcase value="dependentSelect">

												<cfloop array="#local.filters#" index="filterIndex">

													<!--- determine if this is the initial filter load --->
													<cfif len( filterIndex.value )>
														<cfset local.initial = false>
														<cfbreak>
													<cfelse>
														<cfset local.initial = true>
													</cfif>

												</cfloop>

												<cfloop from="1" to="#listlen( arguments.filterFields )#" index="count">

													<cfif count eq 1><cfset local.showNext = true></cfif>

													<cfif local.showNext eq true>

														<cfif local.initial eq true or count gt 1>
															<cfset local.contentIdList = local.finalContentIdList>
														<cfelse>
															<cfset local.contentIdList = local.searchContentIdList>
														</cfif>

														<cfset getKids = getFilterItems( listGetAt( arguments.filterMappedCategory , count ), listGetAt( arguments.filterFields, count ), local.contentIdList )>

														<cfif arrayLen( getKids )>
															<!--- <h2>#listgetat( arguments.filterMappedCategory, count )#</h2> --->
															<cfif len(arguments.filterGroupInnerContainer)><#arguments.filterGroupInnerContainer# #arguments.filterGroupInnerContainerAttributes#></cfif>
																<select name="#local.filters[count].key#" id="filters-#local.filters[count].key#" #arguments.filterAttributes# onChange="this.form.submit();" <cfif arguments.filterType eq 'multiple'>multiple</cfif>>
																	<option value="">Select #listGetAt( arguments.filterMappedCategory , count)#</otpion>
																	<cfloop array="#getKids#" index="kid">
																		<cfset local.thisFilterName = local.filters[count].key>
																		<cfset local.filterValue = local.filters[count].value>
																		<cfif listFind( local.filterValue, kid.catId )>
																			<option value="#kid.catId#" selected="selected">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></option>
																		<cfelse>
																			<option value="#kid.catId#">#kid.name# <cfif arguments.filterCounts eq true>(#kid.count#)</cfif></option>
																		</cfif>
																	</cfloop>
																</select>
															<cfif len(arguments.filterGroupInnerContainer)></#arguments.filterGroupInnerContainer#></cfif>
														</cfif>

														<cfif len( local.filters[count].value )>
															<cfset local.showNext = true>
														<cfelse>
															<cfset local.showNext = false>
														</cfif>
													</cfif>

												</cfloop>
											</cfcase>
											<!--- end: dependent select based filter --->

										</cfswitch>
										<!--- end: display filter --->

									</form>
								<cfif len(arguments.filterGroupContainer)></#arguments.filterGroupContainer#></cfif>
							</#arguments.filterGroupWrapper#>
							<!--- end: filter group wrapper --->

						<cfif len(arguments.filterContainer)></#arguments.filterContainer#></cfif>
						<!--- end: filter container --->

					</#arguments.filterWrapper#>
					<!--- end: filter wrapper --->

				</cfoutput>
			</cfsavecontent>
			<!--- end: rendered filter component --->

		<cfelse>
			<cfset local.ret = "">
		</cfif>
		<!--- end: filters --->

		<cfreturn local.ret>
	</cffunction>
	<!--- end: filter layout --->

	<!--- start: template layout --->
	<cffunction name="getLayoutTemplate" access="public" returntype="string" output="false">
		<cfargument name="iterator" type="any" required="true" hint="Takes in an iterator">
		<cfargument name="preCollectionWrapperHR" type="string" default="false" hint="Put an HR before the collection wrapper">
		<cfargument name="collectionWrapper" type="string" hint="Takes in a tag for the element wrapping the collection, if any.">
		<cfargument name="collectionWrapperAttributes" type="string" hint="Attributes for wrapper. ID, Classes, etc." >
		<cfargument name="collectionContainer" type="string" hint="Takes in a tag for the element container, if any." >
		<cfargument name="collectionContainerAttributes" type="string" hint="Attributes for container. ID, Classes, etc." >
		<cfargument name="heading" type="string" hint="Pass in the entire html block for the heading.">
		<cfargument name="headingDesc" type="string" hint="Pass in the entire html block for the heading description.">
		<cfargument name="collectionBtnLink" type="string" hint="Pass in the link for the button">
		<cfargument name="collectionBtnLabel" type="string" default="Learn More" hint="Pass in the label for the button">
		<cfargument name="collectionBtnAttributes" type="string" default='class="btn btn-default"' hint="Pass in the attributes for the button">
		<cfargument name="collectionInnerContainer" type="string" hint="If we need a row or something like a ul for the content within the container, set it here" >
		<cfargument name="collectionInnerContainerAttributes" type="string" hint="Attributes for the inner container. ID, Classes, etc." >
		<cfargument name="template" type="string" default="default" required="true" hint="Provides a contextual template for the collection. Ex: collection of person vs news templates">
		<cfargument name="fieldlist" type="string" hint="Limits what is displayed based on what's passed in. Obviously this depends on template included.">
		<cfargument name="templateWrapper" type="string" default="article" hint="Takes in a tag for the element wrapping the collection.">
		<cfargument name="templateWrapperAttributes" type="string" hint="Attributes for the template wrapper">
		<cfargument name="imageOrientation" type="string" default="left" hint="Refers how the image will flow around the content: left, right or top.">
		<cfargument name="imageAttributes" type="string" hint="Attributes for the image. How the image will get sized, and any other attributes needed such as width and height.">
		<cfargument name="imageSize" type="string" default="medium" hint="Size for image. Should match what's in the site settings.">

		<cfset local.$ = get$()>  <!--- mura scope --->

		<cfoutput>
			<cfsavecontent variable="ret">

				<cfif arguments.preCollectionWrapperHR><hr></cfif>

				<!--- start: wrapper - create a wrapper, if one is needed --->
				<cfif len(arguments.collectionWrapper)><#arguments.collectionWrapper# #arguments.collectionWrapperAttributes#></cfif>

					<!--- start: container - create a container, if one is needed --->
					<cfif len(arguments.collectionContainer)><#arguments.collectionContainer# #arguments.collectionContainerAttributes#></cfif>

						<!--- start: heading & description --->
						<cfif len(arguments.heading)>#arguments.heading#</cfif> <!--- If we have a heading, show it here --->
						<cfif len(arguments.headingDesc)>#arguments.headingDesc#</cfif> <!--- If we have a heading description, show it here --->
						<!--- end: heading & description --->

						<!--- start: inner container - create a inner container, if one is needed --->
						<cfif len(arguments.collectionInnerContainer)><#arguments.collectionInnerContainer# #arguments.collectionInnerContainerAttributes#></cfif>

							<!--- start: template loop --->
							<cfif arguments.iterator.hasNext()>
								<cfloop condition="arguments.iterator.hasNext()"> <!--- check to see if the iterator has any results, and if so, get the next iterated item --->

									<!--- decide if it has a next value, and save that into a local variable --->
									<cfset "arguments.#template#" = arguments.iterator.next()>

									<!--- iterate each item, and use the Render Sub Type Template to get the layout --->
									<cfinclude template="#$.siteConfig('themeIncludePath')#/display_objects/templates/tpl_#lcase(arguments.template)#.cfm">

								</cfloop>
							</cfif>
							<!--- end: template loop --->

						<cfif len(arguments.collectionInnerContainer)></#arguments.collectionInnerContainer#></cfif>
						<!--- end: container inner --->

						<!--- start: collection button (if necessary) --->
						<cfif len(arguments.collectionBtnLink)>
							<a href="#arguments.collectionBtnLink#" #arguments.collectionBtnAttributes#>#arguments.collectionBtnLabel#</a>
						</cfif>
						<!--- end: collection button --->


					<cfif len(arguments.collectionContainer)></#arguments.collectionContainer#></cfif>
					<!--- end: container --->

				<cfif len(arguments.collectionWrapper)></#arguments.collectionWrapper#></cfif>
				<!--- end: wrapper --->

			</cfsavecontent>
		</cfoutput>

		<cfreturn ret>
	</cffunction>
	<!--- end: template layout --->

	<!--- start: pagination layout --->
	<cffunction name="getLayoutPagination" access="public" returntype="string" output="false">
		<cfargument name="data" required="true" hint="Feed from get data">
		<cfargument name="nextN" type="numeric" default="12" hint="Records per page">
		<cfargument name="startRow" type="numeric" default="1" hint="Starting Row">

		<cfargument name="paginationWrapper" type="string" default="div" hint="Takes in a tag for the element wrapping the paginator, if any.">
		<cfargument name="paginationWrapperAttributes" type="string" default='class="row"' hint="Attributes for pagination wrapper. ID, Classes, etc.">

		<cfargument name="paginationContainer" type="string" default="" hint="Container for pagination component, if necessary">
		<cfargument name="paginationContainerAttributes" type="string" default="" hint="Attributes for container for pagination component">

		<cfargument name="paginationMessageWrapper" type="string" default="h4" hint="Will be wrapped around the pagination message, if necessary">
		<cfargument name="paginationMessageWrapperAttributes" type="string" default='class="page-count col-md-3"' hint="Attributes for the pagination message, if necessary">
		<cfargument name="paginationShowMessage" type="boolean" default="true" hint="Show the message to display next to pagination">
		<cfargument name="paginationMessage" type="string" default="" hint="Message to display next to pagination">

		<cfargument name="paginationInnerContainer" type="string" default="nav" hint="Inner Container for pagination component, if necessary">
		<cfargument name="paginationInnerContainerAttributes" type="string" default='class="page-nav col-md-9"' hint="Attributes for inner container for pagination component">

		<cfargument name="paginationAttributes" type="string" default='class="pagination"' hint="Attributes for inner container for pagination component">

		<cfset local.$ = get$()>  <!--- mura scope --->
		<cfset local.qryStr = ""> <!--- empty list --->

		<!--- Use the utility bean to get all the info about the pagination we need base on our iterator --->
		<cfset local.nextN = local.$.getBean('utility').getNextN( arguments.data.finalFeed.getQuery(), arguments.nextN, arguments.startRow)>

		<cfif arguments.nextN gt 1>
			<cfset local.paginationKey = "startRow">
		<cfelse>
			<cfset local.paginationKey = "pageNum">
		</cfif>

		<cfset local.parsedQryString = _parseFilters( cgi.query_string, '&' )>

		<cfloop array="#local.parsedQryString#" index="pqsIndex">
			<cfif pqsIndex.key neq 'startRow' AND pqsIndex.key neq 'startRow'>
				<cfset local.qryStr = listAppend( local.qryStr, pqsIndex.key & '=' & pqsIndex.value, '&' )>
			</cfif>
		</cfloop>

		<cfif len(arguments.paginationMessage)>
			<cfset local.paginationMessage = arguments.paginationMessage>
		<cfelse>
			<cfset local.paginationMessage = 'Showing <strong>' & local.nextN.currentpagenumber & '</strong> of <strong>' & local.nextN.lastPage & '</strong> pages'>
		</cfif>

		<cfoutput>
			<cfsavecontent variable="ret">

				<!--- start: pagination  wrapper --->
				<cfif local.nextN.lastPage gt 1>
					<#arguments.paginationWrapper# #arguments.paginationWrapperAttributes#>

						<!--- start: pagination container --->
						<cfif len(arguments.paginationContainer)><#arguments.paginationContainer# #arguments.paginationContainerAttributes#></cfif>

							<!--- start: pagination message --->
							<cfif arguments.paginationShowMessage>
								<#arguments.paginationMessageWrapper# #arguments.paginationMessageWrapperAttributes#>#local.paginationMessage#</#arguments.paginationMessageWrapper#>
							</cfif>
							<!--- end: pagination message --->

							<!--- start: pagination inner container --->
							<#arguments.paginationInnerContainer# #arguments.paginationInnerContainerAttributes#>
								<ul #arguments.paginationAttributes#>

									<!--- use the utility bean to loop over content and build the pagination --->
									<cfif local.nextN.currentpagenumber gt 2>
										<li class="navPrev"><a href="#xmlFormat('?#paginationKey#=#local.nextN.previous#&#local.qrystr#')#">&laquo;&nbsp;#local.$.rbKey('list.previous')#</a></li>
									</cfif>

									<cfloop from="#local.nextN.firstPage#" to="#local.nextN.lastPage#" index="i">

										<cfif len($.event('pageNo'))>
											<cfset local.qrystr = "pageNo=#i#"/>
										</cfif>

										<cfif local.nextN.currentpagenumber eq i>
											<li class="active"><span>#i# <span class="sr-only">(current)</span></span></li>
										<cfelse>
											<li><a href="#xmlFormat('?#paginationKey#=#evaluate('(#i#*#local.nextN.recordsperpage#)-#local.nextN.recordsperpage#+1')#&#local.qrystr#')#">#i#</a></li>
										</cfif>

									</cfloop>

									<cfif local.nextN.currentpagenumber lt local.nextN.NumberOfPages>
										<li class="navNext"><a href="#xmlFormat('?#paginationKey#=#local.nextN.next#&#local.qrystr#')#">#local.$.rbKey('list.next')#&nbsp;&raquo;</a></li>
									</cfif>

								</ul>
							</#arguments.paginationInnerContainer#>
							<!--- end: pagination inner container --->

						<cfif len(arguments.paginationContainer)></#arguments.paginationContainer#></cfif>
						<!--- end: pagination container --->

					</#arguments.paginationWrapper#>
				</cfif>
				<!--- end: pagination wrapper --->

			</cfsavecontent>
		</cfoutput>

		<cfreturn ret>
	</cffunction>
	<!--- end: pagination layout --->

<!---
*
* end: layout functions
*
--->


<!---
*
* start: helper functions
*
--->

	<!--- start: data filtering --->
	<cffunction name="getData">

		<!--- start: data, filter and sort args --->
		<cfargument name="data" type="any" hint="Accepts either be empty or feed object">
		<cfargument name="type" type="string" default="Page" hint="Accepts content type (Page, Folder, etc.)">
		<cfargument name="subtype" type="string" default="Default" hint="Accepts sub content type (Default, Folder, etc.)">
		<cfargument name="parentID" type="UUID" hint="Accepts a ParentID to act as a filter source">
		<cfargument name="filename" type="string" hint="Accepts a filename path, if searching a section">
		<cfargument name="contentIDList" type="string" hint="Allows the ability to pass a list of content IDs to filter results by">
		<cfargument name="maxItems" type="numeric" default="0" hint="Max number of results">
		<cfargument name="sortBy" type="string" default="menuTitle" hint="Field to sort by">
		<cfargument name="sortDirection" type="string" default="asc" hint="Direction to sort">

		<!--- search term args --->
		<cfargument name="searchRelatedFields" type="string" hint="Accepts comma-delimited list of related content fields to search on">
		<cfargument name="searchCategoryFields" type="string" hint="Accepts comma-delimited list of category fields or extended attributes that contain category IDs as option lists to search on.">
		<cfargument name="searchFields" type="string" default="title,summary,body" hint="Accepts comma-delimited list of search fields to search on. These should only be normal fields and extended attributes, not related sets or categorys. If we need categories or releated content, simply pass in categories or related as fields to trigger those searches.">
		<cfargument name="searchTerms" type="string" hint="Accepts comma-delimited list of search terms to be searched on">
		<cfargument name="searchOperator" type="string" default="contains" hint="Accepts contains, equals, does not contain, or not equal as operators for search terms">

		<!--- filter args --->
		<cfargument name="filterFields" type="string" hint="Accepts comma-delimited list of fields to be used as search filters">
		<cfargument name="filterRelatedFields" type="string" hint="Accepts comma-delimited list of related content fields to be used as search filters">
		<cfargument name="filters" type="string" default="" hint="Accepts whatever was passed into $.event('filters')">
		<!--- end: data, filter and sort args --->

		<cfset local.$ = get$()>  <!--- mura scope --->
		<cfset local.riList = ""> <!--- related Items var --->
		<cfset local.riFlagTerms = ""> <!--- related terms that get flagged if we found results for related items var --->

		<cfset local.allData = structNew()> <!--- Struct to hold all the different feeds we need --->

		<!--- start: feed or blank --->
		<cfif !isSimpleValue( arguments.data )>
			<cfset local.feed = arguments.data> <!--- no need to make a new feed if where have a feed object passed in --->
		<cfelse>
			<cfset local.feed = $.getBean('feed')> <!--- just set it to a blank feed --->

		</cfif>
		<!--- end: feed or list/blank --->

		<!--- start: filter by type --->
		<cfif len( arguments.type )>
			<cfset local.feed.addParam( field="tcontent.type", criteria=arguments.type, condition="eq" )>
		</cfif>
		<!--- end: filter by type --->

		<!--- start: filter by subType --->
		<cfif len( arguments.subType ) and arguments.subType neq 'Default'>
			<cfset local.feed.addParam( field="tcontent.subtype", criteria=arguments.subtype, condition="eq" )>
		</cfif>
		<!--- end: filter by subType --->

		<!--- start: filter by parentID --->
		<cfif len( arguments.parentID )>
			<cfset feed.addParam( field="tcontent.parentID", criteria=arguments.parentID, condition="eq" )>
		</cfif>
		<!--- end: filter by parentID --->

		<!--- start: filter by filename - if we want to search a section instead of just a parent --->
		<cfif len( arguments.filename )>
			<cfset local.contentBean = $.getBean('content').loadBy(filename=arguments.filename)>
			<cfif !contentBean.getIsNew() >
				<cfset feed.addParam( relationship='AND', field="tcontent.path", dataType='varchar', criteria=local.contentBean.getContentID(), condition="CONTAINS" )>
			</cfif>
		</cfif>
		<!--- end: filter by filename --->

		<!--- start: filter by contentIDList --->
		<!--- if have a list of ContentIDs from the data args, then include them in search results --->
		<cfif len(arguments.contentIDList)>
			<cfset feed.addParam(relationship='andOpenGrouping')>
				<cfloop list="#arguments.contentIDList#" index="node">
					<cfset local.feed.addParam( relationship='OR', field="tcontent.contentID", dataType='varchar', criteria=node, condition="EQUALS" )>
				</cfloop>
			<cfset feed.addParam(relationship='closeGrouping')>
		</cfif>
		<!--- end: filter by contentIDList --->

		<!--- start: max items --->
		<cfif len( arguments.maxItems )>
			<cfset local.feed.setMaxItems(arguments.maxItems)>
		</cfif>
		<!--- end: max items --->

		<!--- start: searchRelatedFields filters --->
		<cfif len( trim(arguments.searchRelatedFields) ) AND len( trim(arguments.searchTerms) )>

			<!--- start: searchRelatedFields loop - loop each related field to find any ContentID's that need to be exlusive to the search --->
			<cfloop list="#arguments.searchRelatedFields#" index="relFieldIndex">

				<!--- start: searchTerms loop - Loop over each search term --->
				<cfloop list="#arguments.searchTerms#" index="riTermIndex" delimiters=", ">
					<cfset local.termIndexList = "" >

					<cfif isValid( "UUID", riTermIndex)>
						<cfset local.riCheck = $.getBean('content').loadBy( contentID = riTermIndex, siteID = $.event('siteID') )>
						<cfset local.riList = listAppend( local.riList, local.riCheck.getContentID() )>
					<cfelse>
						<cfset local.riCheck = $.getBean('feed')>
						<cfset local.riCheck.setSiteID($.event('siteID'))>
						<cfset local.riCheck.addParam( relationship='AND', field='title', dataType='varchar', criteria=riTermIndex, condition='CONTAINS' )>
						<cfset local.riCheck.setMaxItems(0)>
						<cfset local.riCheck.it = local.riCheck.getIterator()>

						<cfloop condition="local.riCheck.it.hasNext()">
							<!--- <cfdump var="#local.riCheck.it.hasNext()#" abort /> --->
							<cfset local.item = local.riCheck.it.next()>
							<cfset local.termIndexList = listAppend( local.termIndexList, local.item.getContentID() )>
						</cfloop>
						<!--- <cfdump var="#local.riCheck.getQuery()#" /> --->
						<!--- <cfdump var="#local.riList#" abort /> --->
					</cfif>

					<cfif listLen(local.termIndexList)>
						<cfset local.relatedItems = getRelatedItems(relFieldIndex,local.termIndexList,riTermIndex)>
						<cfset local.riList = listAppend( local.riList, local.relatedItems.qRet )>
						<cfset local.riFlagTerms = listAppend( local.riFlagTerms, local.relatedItems.riFlagTerms )>
					</cfif>

				</cfloop>
				<!--- end: searchTerms loop --->

				<!--- start: add loop - Then, loop over each related set field, and see if there is a match against the term against a related set --->
				<!--- if have a list of related ContentIDs, then include make sure our search results are filtered --->
				<cfif len(local.riList)>

					<!--- remove any duplicates from the list --->
					<cfset local.riList = listRemoveDuplicates(local.riList) />

					<cfset feed.addParam(relationship='andOpenGrouping')>
						<cfloop list="#local.riList#" index="node">
							<cfset local.feed.addParam( relationship='OR', field="tcontent.contentID", dataType='varchar', criteria=node, condition="EQUALS" )>
						</cfloop>
					<cfset feed.addParam(relationship='closeGrouping')>
				</cfif>
				<!--- end: add loop --->

			</cfloop>
			<!--- end: searchRelatedFields loop --->
		</cfif>
		<!--- end: searchRelatedFields filters --->

		<!--- start: searchCategoryFields filters --->
		<cfif len( trim(arguments.searchCategoryFields) ) AND len( trim(arguments.searchTerms) )>
			<cfset local.feed.addParam(relationship='andOpenGrouping')>

				<!--- start: searchCategoryFields loop - Then, loop over each category field, and see if there is a match against the term directly against a category as opposed through an EA --->
				<cfloop list="#arguments.searchCategoryFields#" index="catFieldIndex">


					<!--- start: searchTerms loop - Loop over each search term --->
					<cfloop list="#arguments.searchTerms#" index="catTermIndex" delimiters=", ">

						<!--- if the keyword is a UUID, then use that to filter as a signal that its hooked to a category  --->
						<cfif isValid("UUID", catTermIndex)>
							<cfset local.feed.setCategoryID(catTermIndex)>

						<!--- otherwise, assume its just text that we're looking to compare the name to --->
						<cfelse>
							<cfset local.keyCat = $.getBean('categoryFeedBean')>
							<cfset local.keyCat.setSiteID($.event('siteID'))>
							<cfset local.keyCat.addParam( relationship='OR', field='tcontentcategories.urltitle', dataType='varchar', criteria=catTermIndex, condition='CONTAINS' )>
							<cfset local.qKeyCat = local.keyCat.getQuery()>
							<cfif local.qKeyCat.recordcount>
								<cfset local.feed.setCategoryID(local.qKeyCat.categoryID)>
							</cfif>
						</cfif>

					</cfloop>
					<!--- end: searchTerms loop --->

				</cfloop>
				<!--- end: searchCategoryFields loop --->

			<cfset local.feed.addParam(relationship='closeGrouping')>
		</cfif>
		<!--- end: searchCategoryFields filters --->


		<!--- start: searchFields filters --->
		<cfif len( trim(arguments.searchFields) ) AND len( trim(arguments.searchTerms) ) AND ( local.riFlagTerms neq arguments.searchTerms )>

			<!--- start: clean up searchTerms --->
			<cfset local.cleanTerm = "" /> <!--- we need an empty list to store clean terms --->
			<cfloop list="#trim(arguments.searchTerms)#" index="seachTermIndex1">

				<!--- start: remove S's and flagged terms from searchTermIndex1 --->
				<cfif lcase(right(seachTermIndex1, 1)) eq "s">
					<cfset seachTermIndex1 = lcase(left(seachTermIndex1,len(seachTermIndex1)-1))>
				</cfif>

				<cfloop list="#seachTermIndex1#" index="flagTermIndex" delimiters=" ">
					<cfif !listFind(local.riFlagTerms,flagTermIndex)>
						<cfset local.cleanTerm = listAppend(local.cleanTerm,flagTermIndex," ") />
					</cfif>
				</cfloop>
				<!--- end: remove S's and flagged terms from searchTermIndex1 --->

			</cfloop>
			<!--- end: clean up searchTerms --->

			<cfset local.preTermsFeedCount = local.feed.getQuery().recordcount /> <!--- lets get a count prior to doing any search terms so that we can compare below --->
			<cfset local.currentParams = duplicate(local.feed.getParams()) /> <!--- get all the current params --->
			<!--- <cfdump var="#local.currentParams#" label="current params before doing first loop"  /> --->

			<!--- start: searchFields loop 1 - loop over each requested search field, and then loop over each search term to check for an exact match --->
			<cfif len(local.cleanTerm)>
				<cfset local.feed.addParam(relationship='andOpenGrouping')>

					<!--- start: searchFields loop --->
					<cfloop list="#arguments.searchFields#" index="fieldIndex1">

						<!--- start: searchTerms --->
						<cfloop list="#trim(local.cleanTerm)#" index="seachTermIndex1">

							<!--- start: keyword type check - if the keyword is a UUID, then use that to filter as a signal that its hooked to a category or another content node --->
							<cfif isValid("UUID", seachTermIndex1)>

								<cfset local.contentIDCheck = $.getBean('content').loadBy( contentID = seachTermIndex1, siteID = $.event('siteID')).getContentID()>
								<cfif len(local.contentIDCheck)>
									<cfset local.feed.addParam( relationship='OR', field='tcontent.contentID', dataType='varchar', criteria=seachTermIndex1, condition='EQUALS' )>
								<cfelse>
										<cfset local.feed.addParam( relationship='OR', field=fieldIndex1, dataType='varchar', criteria=seachTermIndex1, condition='EQUALS' )>
								</cfif>

							<!--- otherwise, assume its just text --->
							<cfelse>

								<!--- check for possible category matches (if we're using categoryIDs in our EA's) --->
								<cfset local.keyCat1 = $.getBean('categoryFeedBean')>
								<cfset local.keyCat1.setSiteID($.event('siteID'))>
								<cfset local.keyCat1.addParam( relationship='OR', field='tcontentcategories.urltitle', dataType='varchar', criteria=seachTermIndex1, condition='CONTAINS' )>
								<cfset local.qKeyCat1 = local.keyCat1.getQuery()>
								<cfif local.qKeyCat1.recordcount>
									<cfset local.feed.addParam( relationship='OR', field=fieldIndex1, dataType='varchar', criteria=local.qKeyCat1.categoryID, condition='EQUALS' )>
								</cfif>

								<!--- otherwise, just check straight text against fieldIndex1 field --->
								<cfset local.feed.addParam( relationship='OR', field=fieldIndex1, dataType='varchar', criteria=seachTermIndex1, condition=arguments.searchOperator )>

							</cfif>
							<!--- end: keyword type check --->

						</cfloop>
						<!--- end: searchTerms loop --->

					</cfloop>

				<cfset local.feed.addParam(relationship='closeGrouping')>

				<!--- start: reset params if nothing found using whole word search --->
				<cfif local.feed.getQuery().recordcount eq 0>
					<cfset local.feed.clearParams() /> <!--- clear all existing params --->
					<cfset local.feed.setParams(local.currentParams) /> <!--- set the params what they were before the first loop --->
				</cfif>
				<!--- end: reset params if nothing found using whole word search --->

			</cfif>
			<!--- end: searchFields loop 1 --->

			<!--- <cfdump var="#local.cleanTerm#" label="clean term?"  />
			<cfdump var="#local.feed.getParams()#" label="current params after doing first loop"  />
			<cfdump var="#local.feed.getQuery().recordcount#" label="records after doing first loop" abort /> --->

			<!--- start: searchFields loop 2 --->
			<cfif len(local.cleanTerm) AND (local.feed.getQuery().recordcount eq 0 OR local.feed.getQuery().recordcount eq local.preTermsFeedCount)>

				<!--- start: multiple term search --->
				<cfset local.feed.addParam(relationship='andOpenGrouping')>

					<!--- start: searchFields loop - loop over each requested search field, and then loop over each search term to check for a match from each term --->
					<cfloop list="#arguments.searchFields#" index="fieldIndex2">

						<!--- start: searchTerms loop--->
						<cfloop list="#arguments.searchTerms#" index="seachTermIndex2" delimiters=" ">

							<!--- start: flagged terms --->
							<cfif !listFind(local.riFlagTerms,seachTermIndex2)>

								<cfif lcase(right(seachTermIndex2, 1)) eq "s">
									<cfset seachTermIndex2 = lcase(left(seachTermIndex2,len(seachTermIndex2)-1))>
								</cfif>

								<!--- if the keyword is a UUID, then use that to filter as a signal that its hooked to a category or another content node --->
								<cfif isValid("UUID", seachTermIndex2)>

									<cfset local.contentIDCheck = $.getBean('content').loadBy( contentID = seachTermIndex2, siteID = $.event('siteID')).getContentID()>
									<cfif len(local.contentIDCheck)>
										<cfset local.feed.addParam( relationship='OR', field='tcontent.contentID', dataType='varchar', criteria=seachTermIndex2, condition='EQUALS' )>
									<cfelse>
											<cfset local.feed.addParam( relationship='OR', field=fieldIndex2, dataType='varchar', criteria=seachTermIndex2, condition='EQUALS' )>
									</cfif>
								<!--- otherwise, assume its just text --->
								<cfelse>

									<!--- check for possible category matches (if we're using categoryIDs in our EA's) --->
									<cfset local.keyCat2 = $.getBean('categoryFeedBean')>
									<cfset local.keyCat2.setSiteID($.event('siteID'))>
									<cfset local.keyCat2.addParam( relationship='OR', field='tcontentcategories.urltitle', dataType='varchar', criteria=seachTermIndex2, condition='CONTAINS' )>
									<cfset local.qKeyCat2 = local.keyCat2.getQuery()>
									<cfif local.qKeyCat2.recordcount>
										<cfset local.feed.addParam( relationship='OR', field=fieldIndex2, dataType='varchar', criteria=local.qKeyCat2.categoryID, condition='EQUALS' )>
									</cfif>

									<!--- otherwise, just check straight text against fieldIndex2 field --->
									<cfset local.feed.addParam( relationship='OR', field=fieldIndex2, dataType='varchar', criteria=seachTermIndex2, condition=arguments.searchOperator )>
								</cfif>

							</cfif>
							<!--- end: flagged terms --->

						</cfloop>
						<!--- end: searchTerms loop --->

					</cfloop>
					<!--- end: searchFields loop --->

				<cfset local.feed.addParam(relationship='closeGrouping')>
				<!--- end: multiple term search --->

			</cfif>
			<!--- end: searchFields loop 2 --->

			<!--- <cfdump var="#!len(local.cleanTerm)#" label="clean term?"  />
			<cfdump var="#local.feed.getParams()#" label="current params after doing 2nd loop"  />
			<cfdump var="#local.feed.getQuery().recordcount#" label="records after doing 2nd loop" abort /> --->


			<!--- start: reset params if nothing found using single word search --->
			<cfif local.feed.getQuery().recordcount eq 0>
				<cfset local.feed.clearParams() /> <!--- clear all existing params --->
				<cfset local.feed.setParams(local.currentParams) /> <!--- set the params what they were before the first loop --->
			</cfif>
			<!--- end: reset params if nothing found using single word search --->

			<!--- start: scrub loop 2 --->
			<!--- <cfset local.currentParams = scrubEmptyParamRelationships(local.feed.getParams()) /> ---> <!--- make sure that don't have any empty param relationships --->
			<!--- <cfset local.feed.setParams(local.currentParams) /> ---> <!--- set the scrubbed params and set them to the current params after the second loop --->
			<!--- end: scrub loop 2 --->

		</cfif>
		<!--- end: searchFields filters --->

		<cfset local.allData.searchedFeedQuery = local.feed.getQuery()>

		<!--- start: filters --->
		<cfset local.isFilter = false><!--- set default variables --->
		<cfset local.parsedFilters = _parseFilters( arguments.filters )><!--- parse the existing filters to get a key value pair structure inside array elements --->

		<!--- loop over parsedFilters array to see if any of the filters have a value, if yes, then we'll filter, otherwise no need to filter --->
		<cfloop array="#local.parsedFilters#" index="local.pf">
			<cfif len( local.pf.value )>
				<cfset local.isFilter = true>
				<cfbreak>
			</cfif>
		</cfloop>

		<!--- some value in the array of structs has a real value, so let's filter --->
		<cfif local.isFilter eq true>

			<!--- reset local loopers --->
			<cfset local.pf = "">
			<cfset local.ff = "">
			<cfset local.fv = "">

			<!--- start: loop parsedFilters --->
			<cfloop array="#local.parsedFilters#" index="local.pf">

				<cfset local.filterValue = local.pf.value>

				<!--- start: loop over each filter value --->
				<cfloop list="#local.filterValue#" delimiters=":" index="local.fv">
					<cfset local.feed.addParam(relationship="andOpenGrouping") />

					<cfloop list="#arguments.filterFields#" index="local.ff">
						<cfset local.filterField = local.ff>

						<!--- check to see if filterField is a category --->
						<cfset local.filterCat = $.getBean('category').loadBy( name = local.filterField , siteId = $.event('siteID') )>

						<cfif local.filterCat.getIsNew() eq 0>
							<!--- local.filterField exists as a category, so we'll search on category --->
							<cfset local.filterAddParamField = 'tcontentcategoryassign.categoryId'>
						<cfelse>
							<cfset local.filterAddParamField = local.filterField>
						</cfif>

						<cfset local.feed.addParam( field=#local.filterAddParamField#, criteria=trim(local.fv), relationship='OR', condition='CONTAINS')>

					</cfloop>

					<cfset local.feed.addParam(relationship="closeGrouping")>
				</cfloop>
				<!--- end: loop over each filter value --->

			</cfloop>
			<!--- end: loop parsedFilters --->
		</cfif>
		<!--- end: filters --->

		<!--- start: feed additionals --->
		<cfset local.feed.setName('temp')>
		<cfset local.feed.setSiteID($.event('siteId'))>
		<cfset local.feed.setIsActive(1)>
		<cfset local.feed.setSortBy( arguments.sortBy )>
		<cfset local.feed.setSortDirection( arguments.sortDirection )>
		<!--- end: feed additionals --->

		<cfset local.allData.finalFeed = local.feed>

		<cfreturn local.allData>

		<!--- <cfdump var="#local.feed.getQuery()#" abort />
		<cfdump var="#arguments.data#" label="default data" />
		<cfdump var="#local.feed.getQuery()#" label="query from data arg" />
		<cfdump var="#local.feed.getRecordCount()#" label="feed count data" />
		<cfdump var="#!isSimpleValue( arguments.data )#" label="Simple Value"  /> --->
	</cffunction>
	<!--- end: data filtering --->

	<!--- start: get related items --->
	<cffunction name="getRelatedItems" output="false" returntype="any">
		<cfargument name="set" required="true">
		<cfargument name="riList" required="true">
		<cfargument name="term" required="true">

		<cfset local.$ = get$()>
		<cfset s = StructNew()>
		<cfset local.riFlagTerms = ''>

		<cfset local.ri = local.$.getBean('feed')>
		<cfset local.ri.setSiteID($.event('siteID'))>
		<cfset local.ri.setName('riTemp')>
		<cfset local.ri.setIsActive(1)>
		<cfset local.ri.setMaxItems(0)>
		<cfset local.ri.setNextN(9999)>
		<cfset local.ri.addParam( relationship='AND', field="tcontentrelated.relatedcontentsetid", dataType='varchar', criteria=arguments.set, condition='EQUALS')>

		<cfset local.ri.addParam(relationship='andOpenGrouping')>
		<cfloop list="#riList#" index="i">
			<cfset local.ri.addParam( relationship='OR', field="tcontentrelated.relatedid", dataType='varchar', criteria=trim(i), condition='EQUALS')>
		</cfloop>
		<cfset local.ri.addParam(relationship='closeGrouping')>

		<cfset q = local.ri.getQuery()>
		<cfset s.qRet = valueList(q.contentID)>

		<!--- if we have any related items based on the search term, then flag this term, so we don't try to search on it --->
		<cfif len(s.qRet)>
			<cfset local.riFlagTerms = listAppend( local.riFlagTerms, arguments.term )>
		</cfif>
		<cfset s.riFlagTerms = local.riFlagTerms>

		<cfreturn s>
	</cffunction>
	<!--- end: get related items --->

	<!--- start: scrub empty param relationships --->
	<cffunction name="scrubEmptyParamRelationships" output="false" returntype="query">
		<cfargument name="query" required="true">

		<cfset local.openList = "" />
		<cfset local.closeList = "" />
		<cfset local.removeList = "" />

		<cfloop query="arguments.query">
			<cfif relationship eq "andOpenGrouping">
				<cfset local.openList = listAppend(local.openList,param) />
			<cfelseif relationship eq "closeGrouping">
				<cfset local.closeList = listAppend(local.closeList,param) />
			</cfif>
		</cfloop>

		<cfloop list="#local.openList#" index="openIndex">
			<cfloop list="#local.closeList#" index="closeIndex">
				<cfif (closeIndex - openIndex) eq 1 >
					<cfset local.removeList = listAppend(local.removeList,openIndex) />
					<cfset local.removeList = listAppend(local.removeList,closeIndex) />
				</cfif>
			</cfloop>
		</cfloop>

		<cfset local.removeList = listSort(local.removeList,"numeric","desc") />

		<cfloop list="#local.removeList#" index="removeIndex">
			<cfset arguments.query.deleteRow(removeIndex) />
		</cfloop>

		<cfreturn arguments.query>
	</cffunction>
	<!--- end: scrub empty param relationships --->

	<!--- start: filter functions --->
	<cffunction name="_buildExistingFilters" output="false">
		<cfargument name="currentFilter" required="true">
		<cfargument name="currentFilterId" required="true">
		<cfargument name="allFilters" required="true">
		<cfargument name="filterType" required="true">
		<cfargument name="searchTerms" required="true">

		<cfset local.outsideFilter = "">
		<cfset local.currentFilter = "">
		<cfset local.c = 1>

		<cfloop array="#arguments.allFilters#" index="i">
			<cfswitch expression="#arguments.filterType#">
				<cfcase value="single">

					<!--- key and currentfilter are the same, so since we're in single, replace whatever we've already got --->
					<cfloop from="1" to="1" index="x">
						<cfif i.key eq arguments.currentFilter>

							<!--- only add the item back to the if the item is currently NOT equal to the value --->
							<cfif i.value neq arguments.currentFilterId>
								<cfset local.currentFilter = local.currentFilter & arguments.currentFilterId>
							</cfif>

							<cfbreak>
						<cfelse>
							<!--- key and currentfilter are not the same, so set other filter values --->
								<cfset local.outsideFilter = local.outsideFilter & i.key & '=' & i.value>
						</cfif>
					</cfloop>

				</cfcase>
				<cfcase value="multiple">
				<!--- are we working with the current filter or 'other' filter --->
					<cfif i.key neq arguments.currentFilter>
						<cfset local.outsideFilter = local.outsideFilter & i.key & '=' & i.value>
					<cfelse>
						<!--- if i.value isn't the same as currentFilterId, add it to the string --->
						<cfif not listfind( i.value, arguments.currentFilterId, ':' )  >
							<!--- build current filter --->
							<cfset local.currentFilter = local.currentFilter & i.value & ':' & arguments.currentFilterId>
						<cfelse>
							<cfset local.currentFilter = local.currentFilter & listDeleteAt( i.value, listFindNoCase( i.value, arguments.currentFilterId, ':' ), ':' ) >
						</cfif> <!--- // i.value neq arguments.currentFilterId --->

					</cfif> <!--- // i.key neq arguments.currentFilter --->
				</cfcase>
				<cfdefaultcase>
				</cfdefaultcase>
			</cfswitch>

		</cfloop>

		<cfif left( local.currentFilter, '1' ) eq ':'>
			<cfset local.currentFilter = right( local.currentFilter, len(local.currentFilter)-1)>
		</cfif>

		<cfif left( local.outsideFilter, '1' ) eq ':'>
			<cfset local.outsideFilter = right( local.outsideFilter, len(local.outsideFilter)-1)>
		</cfif>

		<cfset local.finalFilter = "">

		<cfif len( local.currentFilter )>
			<cfset local.finalFilter = local.currentFilter>
		</cfif>

		<cfif len( local.outsideFilter )>
			<cfset local.finalFilter = local.finalFilter & '&' & local.outsideFilter>
		</cfif>

		<cfset local.finalFilter = local.finalFilter & '&keywords=' & arguments.searchTerms>

		<cfreturn local.finalFilter>
	</cffunction>

	<cffunction name="_parseFilters" access="private" returntype="Array" output="false">
		<cfargument name="filterString" required="true" hint="this is full string of what is being currently filtered (field names and values)">
    <cfargument name="delim" required="false" default=",">

			<cfset local.keyValuePairsArray = arrayNew(1)>

			<cfloop list="#trim(arguments.filterString)#" index="i" delimiters="#arguments.delim#">
				<cfset local.key = listFirst( i, '=')>
				<cfset local.value = listLast( i, '=')>

				<cfset local.tempStruct = structNew()>
					<cfset local.tempStruct.key = local.key>

				<cfif right( local.value, 1 ) eq '=' or local.value eq local.key>
					<cfset local.tempStruct.value = ''>
				<cfelse>
					<cfset local.tempStruct.value = local.value>
				</cfif>

				<cfset arrayAppend( local.keyValuePairsArray, local.tempStruct )>

			</cfloop>

		<cfreturn local.keyValuePairsArray>
	</cffunction>

	<cffunction name="getFilterItems" output="false">
		<cfargument name="categoryName" required="true">
		<cfargument name="attributeName" required="true">
		<cfargument name="contentIdList" required="true">

		<cfset local.$ = get$()>
		<cfset local.arrResults = ArrayNew(1)>

		<cfset tlCats = getBean('category').loadBy(name=arguments.categoryName, siteId = local.$.event('siteId') ).getKidsQuery() >

		<!--- check to see if attributeName is a category --->
		<cfset catCheck = getBean('category').loadBy( name=arguments.attributeName, siteId = local.$.event('siteId') ) >

		<!--- isnew eq 0 (already exists) then look for value as categoryId --->
		<cfif catCheck.getIsNew() eq 0>
			<cfset local.catCounts = renderCategoryCounts( catCheck.getCategoryId(), arguments.contentIdList )>
			<cfset local.arrResults = local.catCounts>
		<cfelse>
			<cfloop query="tlCats">
				<cfset local.eaCounts = renderExtendedAttributeCounts(arguments.contentIdList, arguments.attributeName, tlCats.name, tlCats.categoryId )>
				<cfif structKeyExists( local.eaCounts, "catId" )>
					<cfset local.temp = ArrayAppend( local.arrResults, local.eaCounts )>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn local.arrResults>
	</cffunction>

	<cffunction name="renderExtendedAttributeCounts" output="false">
		<cfargument name="contentIdList">
		<cfargument name="attributeName">
		<cfargument name="categoryName">
		<cfargument name="categoryId">

		<cfset local.$ = get$()>
		<cfset local.itemStruct = StructNew()>
		<cfset local.arrResults = arrayNew(1)>

		<cfset local.feed = local.$.getBean('feed')>
			<cfset local.feed.setMaxItems(0)>

			<cfset local.feed.addParam( field='contentid', criteria=arguments.contentIdList , condition="IN", relationship="and" )>
			<cfset local.feed.addParam( field=arguments.attributeName, criteria=arguments.categoryId, condition="CONTAINS" )>
			<cfset local.q = local.feed.getQuery()>

			<cfif local.q.recordcount gt 0>
				<cfset local.itemStruct.catId = arguments.categoryId>
				<cfset local.itemStruct.name = arguments.categoryName>
				<cfset local.itemStruct.count = local.q.recordcount>
			</cfif>

		<cfreturn local.itemStruct>
	</cffunction>

	<cffunction name="renderCategoryCounts" output="false">
		<cfargument name="parentCategoryId" required="true">
		<cfargument name="contentIdList" required="true">

			<cfset local.$ = get$()>

			<cfset arrResults = ArrayNew(1)>
			<cfset runningTotal = 0>

			<cfset tlCats = local.$.getBean('category').loadBy(categoryId=arguments.parentCategoryId, siteId = local.$.siteConfig().getSiteId() ).getKidsQuery() >

			<cfloop query="tlCats">

				<cfset kidsCats = local.$.getBean('category').loadBy(categoryId=tlCats.categoryId, siteId = local.$.event('siteId')).getKidsQuery() >

				<cfquery name="rs" datasource="#application.configBean.getDatasource()#" username="#application.configBean.getDBUsername()#" password="#application.configBean.getDBPassword()#">
					SELECT sum(myCount) as mySum FROM(
					SELECT
						cats.categoryID,
						cats.filename,
						Count(c.contenthistID) as "myCount",
						cats.name
					FROM
						tcontent c
					INNER JOIN tcontentcategoryassign catsa
					ON (c.contenthistID=catsa.contentHistID
						and c.siteID=catsa.siteID)
					INNER JOIN tcontentcategories cats
					ON (catsa.categoryID=cats.categoryID
						AND catsa.siteID=cats.siteID)
					WHERE
						cats.isActive=1
						AND c.active = 1
						AND c.approved = 1
						AND c.moduleid = '00000000000000000000000000000000000'
						AND c.isNav = 1
						AND c.siteid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#$.event('siteId')#"/>
						AND catsa.categoryId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#tlCats.categoryId#"/>
						AND c.contentId IN (<cfqueryparam list="true" value="#arguments.contentIdList#">)
					GROUP BY cats.name,cats.categoryID,cats.filename ) x
				</cfquery>

				<cfif rs.recordcount eq 0 or not len(rs.mySum)>
					<cfset ct = 0>
				<cfelse>
					<cfset ct = rs.mySum>
				</cfif>

				<cfset stCat = StructNew()>
					<cfset stCat.name = tlCats.name>
					<cfset stCat.catid = tlCats.categoryId>
					<cfset stCat.count = ct>

					<cfset runningTotal = runningTotal + ct>
					<cfif stCat.count gt 0>
						<cfset arrayAppend(arrResults, stCat)>
					</cfif>
			</cfloop>

		<cfreturn arrResults>
	</cffunction>
	<!--- end:  filter functions --->

	<!--- start: mura scope --->
	<cffunction name="get$" output=false>
		<cfscript>
			return StructKeyExists(variables, '$') ? variables.$ : application.serviceFactory.getBean('$').init(session.siteid);
		</cfscript>
	</cffunction>
	<!--- end: mura scope --->

<!---
*
* end: helper functions
*
--->
</cfcomponent>