<theme>
	<imagesizes>
		<imagesize name="carouselimage" width="1600" height="900" />
	</imagesizes>

  <extensions>

    <!--- start: Base Object --->
    <extension type="Base">
      <relatedcontentset name="Related Blogs" availableSubTypes="Page/Blog" />
      <relatedcontentset name="Related FAQs" availableSubTypes="Page/Question" />
      <relatedcontentset name="Related Images" availableSubTypes="File/Default" />
      <relatedcontentset name="Related Organizations" availableSubTypes="Page/Organization" />
      <relatedcontentset name="Related People" availableSubTypes="Page/Person" />
      <relatedcontentset name="Related Products" availableSubTypes="Page/Product" />
      <relatedcontentset name="Related Testimonials" availableSubTypes="Page/Testimonial" />
    </extension>
    <!--- end: Base Object --->

    <!--- start: folders --->
    <extension type="Folder" subType="Blogs" availableSubTypes="Page/Blog"></extension>
    <extension type="Folder" subType="FAQs" availableSubTypes="Page/Question"></extension>
    <extension type="Folder" subType="Organizations" availableSubTypes="Page/Organization"></extension>
    <extension type="Folder" subType="Products" availableSubTypes="Page/Product"></extension>
    <extension type="Folder" subType="Team" availableSubTypes="Page/Person"></extension>
    <extension type="Folder" subType="Testimonials" availableSubTypes="Page/Testimonial"></extension>
    <!--- end: folders --->


    <!--- start: Blog --->
    <extension type="Page" subtype="Blog"></extension>
    <!--- end: Blog --->

    <!--- start: Product --->
    <extension type="Page" subtype="Product">
      <attributeset name="Product Attributes" container="Basic">
       	<attribute
          name="productType"
          label="Product Type"
          hint="Choose a Type for your Product"
          type="SelectBox"
          defaultValue="Bracelet"
          optionList="Bracelet^Necklace^Clip^Earring"
          optionLabelList="Bracelet^Necklace^Clip^Earring"
          required="true" />

        <attribute
          name="productSubType"
          label="Product Sub Type"
          hint="Choose a Sub Type for your Product"
          type="MultiSelectBox"
          defaultValue="Braided"
          optionList="Braided^Choker^Hexagon^NA^Thick Strip^Thin Strip^Wrap"
          optionLabelList="Braided^Choker^Hexagon^NA^Thick Strip^Thin Strip^Wrap" />

        <attribute
          name="productPrice"
          label="Product Price"
          hint="Choose a Price for your Product"
          type="Text" />

        <attribute
          name="productColor"
          label="Product Color"
          hint="Choose a Color for your Product"
          type="SelectBox"
          defaultValue="NA"
          optionList="Beige^Black^Black Brown^Blue^Brown^Gray^Green^Mahogany^Pink^Light Purple^Yellow"
          optionLabelList="Beige^Black^Black Brown^Blue^Brown^Gray^Green^Mahogany^Pink^Light Purple^Yellow" />

        <attribute
          name="productSize"
          label="Product Size"
          hint="Choose a Size for your Product"
          type="SelectBox"
          defaultValue="NA"
          optionList="NA^Extra Small^Small^Medium^Large"
          optionLabelList="NA^Extra Small^Small^Medium^Large" />

        <attribute
          name="productLength"
          label="Product Length"
          hint="Choose a Length for your Product"
          type="Text" />

        <attribute
          name="productWidth"
          label="Product Width"
          hint="Choose a Width for your Product"
          type="Text" />

        <attribute
          name="productMaterial"
          label="Product Material"
          hint="Choose a Material for your Product"
          type="MultiSelectBox"
          defaultValue="Faux Suede"
          optionList="Leather^Faux Suede^Brass"
          optionLabelList="Leather^Faux Suede^Brass" />

        <attribute
          name="productMaterialDescription"
          label="Product MaterialDescription"
          hint="Choose a MaterialDescription for your Product"
          type="HTMLEditor" />

        <attribute
          name="productAmazonLink"
          label="Product Amazon Link"
          hint="Choose a Amazon Link for your Product"
          type="Text" />

        <attribute
          name="productSku"
          label="Product Sku"
          hint="Choose a Sku for your Product"
          type="Text" />
      </attributeset>
    </extension>
    <!--- end: Product --->

    <!--- PAGE - person --->
    <extension type="Page" subtype="Person">
      <attributeset name="Person Options" container="Custom">

        <attribute
          name="personFirstName"
          label="First Name"
          hint="Enter First Name"
          required="false" />

        <attribute
          name="personLastName"
          label="Last Name"
          hint="Enter Last Name"
          required="false" />

        <attribute
          name="personTitle"
          label="Title of Person"
          hint="Enter Person's Title (Chef, Lawyer, Doctor, etc.)"
          required="false" />

        <attribute
          name="personCredentials"
          label="Credentials of Person"
          hint="Enter Person's Credentials (MD, Ph.D, JD, etc.)"
          required="false" />


        <attribute
          name="personPostal"
          label="Postal Code"
          hint="Enter the Postal Code of the Person"
          required="false" />

        <attribute
          name="personPhone"
          label="Phone"
          hint="Enter the phone number of the Person"
          required="false" />

        <attribute
          name="personWeb"
          label="Web"
          hint="Enter the website of the Person"
          required="false" />

        <attribute
          name="personEmail"
          label="Email"
          hint="Enter the email of the Person"
          required="false"
          validation="Email" />

        <attribute
          name="personFacebook"
          label="Facebook Page"
          hint="Enter the link to the Facebook Page for the Person"
          required="false" />

        <attribute
          name="personTwitter"
          label="Twitter Page"
          hint="Enter the link to the Twitter Page for the Person"
          required="false" />

        <attribute
          name="personPinterest"
          label="Pinterest Page"
          hint="Enter the link to the Pinterest Page for the Person"
          required="false" />

        <attribute
          name="personInstagram"
          label="Instagram Page"
          hint="Enter the link to the Instagram Page for the Person"
          required="false" />

        <attribute
          name="personGoogle+"
          label="Google+ Page"
          hint="Enter the link to the Google+ Page for the Person"
          required="false" />

      </attributeset>
    </extension>

    <!--- start: Organization --->
    <extension type="Page" subtype="Organization">
      <attributeset name="Organization Attributes" container="Basic">

        <attribute
          name="orgStatus"
          label="Organization Status"
          hint="Choose a Status for the organization"
          type="SelectBox"
          defaultValue="Pending"
          optionList="Approved^Pending"
          optionLabelList="Approved^Pending" />

        <attribute
          name="orgType"
          label="Organization Type"
          hint="Choose a type for the organization"
          type="MultiSelectBox"
          defaultValue="Vendor"
          optionList="Vendor^Collaborator"
          optionLabelList="Vendor^Collaborator" />

        <attribute
          name="orgAddress1"
          label="Address Line 1"
          hint="Enter Address Line 1 of the Organization"
          type="TextBox"
          required="false" />

         <attribute
          name="orgAddress2"
          label="Address Line 2"
          hint="Enter Address Line 2 of the Organization (ex. building number, etc.)"
          type="TextBox"
          required="false" />

        <attribute
          name="orgCity"
          label="City"
          hint="Enter the City of the Organization"
          type="TextBox"
          required="false" />

        <attribute
          name="orgState"
          label="Organization State"
          hint="Select the Organizations State"
          type="SelectBox"
          required="false"
          defaultValue="CA"
          optionList="CA^OR"
          optionLabelList="CA^OR" />

        <attribute
          name="orgPostal"
          label="Postal Code"
          hint="Enter the Postal Code of the Organization"
          type="TextBox"
          required="false" />

        <attribute
          name="orgPhone"
          label="Phone"
          hint="Enter the phone number of the Organization"
          type="TextBox"
          required="false" />

        <attribute
          name="orgWeb"
          label="Web"
          hint="Enter the website address of the Organization"
          type="TextBox" />

        <attribute
          name="orgEmail"
          label="Email"
          hint="Enter the email of the Organization"
          type="TextBox"
          required="false"
          validation="Email"
          message="Please enter a valid Organization Email" />

        <attribute
          name="orgFacebook"
          label="Facebook Page"
          hint="Enter the link to the Facebook page for the Organization"
          type="TextBox"
          required="false" />

        <attribute
          name="orgTwitter"
          label="Twitter Page"
          hint="Enter the link to the Twitter page for the Organization"
          type="TextBox"
          required="false" />

        <attribute
          name="orgInstagram"
          label="Instagram Page"
          hint="Enter the link to the Google+ page for the Organization"
          type="TextBox"
          required="false" />

        <attribute
          name="orgPartner"
          label="Prefered Partner"
          hint="Is this Organization a Partner?"
          type="SelectBox"
          required="false"
          defaultValue="1"
          optionList="1^0"
          optionLabelList="Yes^No" />

      </attributeset>
    </extension>
    <!--- end: Organization --->

    <!--- start: Question --->
    <extension type="Page" subtype="Question">
      <attributeset name="Question Attributes" container="Basic">
        <attribute
          name="questionType"
          label="Question Type"
          type="SelectBox"
          required="false"
          defaultValue=""
          optionList="General^Privacy^Product"
          optionLabelList="General^Privacy^Product" />

      </attributeset>
    </extension>
    <!--- end: Question --->

    <!--- start: Testimonial --->
    <extension type="Page" subtype="Testimonial"></extension>
    <!--- end: Testimonial --->

  </extensions>

</theme>
