<!--- Get Featured and Slideshow content for homepage --->
<cfsilent>

  <cfset local.itServices = $.content().getRelatedContentIterator(name='Related Services')>

  <!--- start: heading & desc --->
  <cfset local.serviceTitle = '<header><h2 class="intro">' & $.content('contentTagLine') & '</h2><h1>' & $.content('title') & '</h1></header>' />
  <cfif len($.content('summary'))>
    <cfset local.serviceDesc = '<div class="lead">' & $.content('summary') & '</div><hr>' />
  <cfelse>
    <cfset local.serviceDesc = "">
  </cfif>
  <!--- end: heading & desc --->


  <cfset local.itProjects = $.content().getRelatedContentIterator(name='Related Projects')>

</cfsilent>

<cfoutput>

  <cfinclude template="inc/html_head.cfm">
  <cfinclude template="inc/header.cfm">

    <!--- start: slider --->
    <cfinclude template="../display_objects/content-modules/dsp_cm_fullslider.cfm">
    <!--- end: slider --->

    <!--- start: main content --->
    <section id="nextSection" role="main">

      <!--- start: body content --->
      #$.dspBody(body='',pageTitle='',crumbList=0,showMetaImage=0)#
      <!--- end: body content --->

      <!--- start: featured services --->
      <cfif local.itServices.hasNext()>

        <!--- start: iterator --->
        #$.renderCollection.getLayoutTemplate(
          iterator                             = local.itServices
          , collectionWrapper                  = 'section'
          , collectionWrapperAttributes        = 'class="content-area container text-center"'
          , collectionContainer                = ''
          , collectionContainerAttributes      = ''
          , heading                            = local.serviceTitle
          , headingDesc                        = local.serviceDesc
          , collectionBtnLink                  = '/about/'
          , collectionBtnAttributes            = 'class="btn btn-primary"'
          , collectionBtnLabel                 = 'Learn Who We Are'
          , collectionInnerContainer           = 'div'
          , collectionInnerContainerAttributes = 'class="row" role="service-collection"'
          , template                           = 'service'
          , fieldlist                          = ''
          , templateWrapper                    = 'article'
          , templateWrapperAttributes          = 'class="col-md-4 animated activate fadeInUp" data-fx="fadeInUp"'
          , imageOrientation                   = 'top'
          , imageAttributes                    = ''
          , imageSize                          = ''
        )#
        <!--- end: iterator --->

      </cfif>
      <!--- end: featured services --->


      <!--- start: featured projects --->
      <cfif local.itProjects.hasNext()>
        <section class="content-area">
          <div class="bg-img" style="background: url(#$.getImageUrl('carouselimage')#);"></div>

            <!--- start: iterator --->
            #$.renderCollection.getLayoutTemplate(
              iterator                             = local.itProjects
              , collectionWrapper                  = 'div'
              , collectionWrapperAttributes        = 'class="container text-center"'
              , collectionContainer                = ''
              , collectionContainerAttributes      = ''
              , heading                            = '<header><h2 class="intro">Our Latest Work</h2><h1>Check Out Our Recent Projects</h1></header>'
              , collectionBtnLink                  = '/projects/'
              , collectionBtnLabel                  = 'View Our Projects'
              , collectionBtnAttributes            = 'class="btn btn-primary"'
              , collectionInnerContainer           = 'div'
              , collectionInnerContainerAttributes = 'class="row"'
              , template                           = 'default'
              , fieldlist                          = ''
              , templateWrapper                    = 'article'
              , templateWrapperAttributes          = 'class="col-sm-4 animated activate fadeInUp" data-fx="fadeInUp"'
              , imageOrientation                   = 'top'
              , imageAttributes                    = ''
              , imageSize                          = 'small'
            )#
            <!--- end: iterator --->
        </section>
      </cfif>
      <!--- end: featured projects --->


    </section>
  <cfinclude template="inc/footer.cfm" />
  <cfinclude template="inc/html_foot.cfm" />
</cfoutput>