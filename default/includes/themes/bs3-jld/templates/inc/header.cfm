<cfoutput>
<body id="#$.getTopID()#" class="#$.createCSSid($.content('menuTitle'))#">

	<cfif $.getcontentID() eq "00000000000000000000000000000000001"><div id="boxedWrapper"></cfif>

	<!--[if lte IE 8]>
		<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> to better experience this site.</p>
	<![endif]-->

	<!--- start: header --->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">

				<!--- start: branding --->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
					<h1>
						<a href="/" title="#HTMLEditFormat($.siteConfig('site'))#">
							<img src="#$.siteConfig('themeAssetPath')#/images/logo.png" alt="#HTMLEditFormat($.siteConfig('site'))#" />#HTMLEditFormat($.siteConfig('site'))#
						</a>
					</h1>
		    </div>
				<!--- end: branding --->

    		<!--- start: primary navigation --->
				<div class="collapse navbar-collapse">
					<cf_CacheOMatic key="dspPrimaryNav">
						<!---
							For information on dspPrimaryNav(), visit:
							http://docs.getmura.com/v6/front-end/template-variables/document-body/
						--->
						#$.dspPrimaryNav(
							viewDepth=1
							, id='navPrimary'
							, class='nav navbar-nav navbar-right'
							, displayHome='always'
							, closeFolders=false
							, showCurrentChildrenOnly=false
							, liHasKidsClass='dropdown'
							, liHasKidsAttributes=''
							, liCurrentClass=''
							, liCurrentAttributes=''
							, liHasKidsNestedClass='dropdown-submenu'
							, aHasKidsClass='dropdown-toggle'
							, aHasKidsAttributes='role="button" data-toggle="dropdown" data-target="##"'
							, aCurrentClass=''
							, aCurrentAttributes=''
							, ulNestedClass='dropdown-menu'
							, ulNestedAttributes=''
							, aNotCurrentClass=''
							, siteid=$.event('siteid')
						)#
					</cf_CacheOMatic>
					<script>
						$(function(){
							$(#serializeJSON($.getCurrentURLArray())#).each(
								function(index, value){
									$("##navPrimary [href='" + value + "']").closest("li").addClass("active");
								}
							);
						})
					</script>
				</div>
    		<!--- end: primary navigation --->

			</div>
		</nav>
	</header>
	<!--- end: header --->

</cfoutput>

