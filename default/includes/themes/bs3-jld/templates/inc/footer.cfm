<cfoutput>

	<!--- start: footer --->
	<footer class="footer-wrapper">

		<!--- start: testimonials --->
	  <!--- <cfinclude template="../../display_objects/content-modules/dsp_cm_testimonials.cfm"> --->
		<!--- end: testimonials --->

		<!--- start: primary footer --->
	  <section class="container">

			<div class="row">

				<!--- start: about us --->
	      <section class="col-sm-8">
	        <!--- gets the content bean from the About section --->
	        <cfset footerAbout=$.getBean("content").loadBy(contentID="BE5EE374-659D-45AD-B4A7E3B9631F4D6C",siteID=$.event("siteid"))>
	        <h1>#footerAbout.getTitle()#</h1>
	        #footerAbout.getSummary()#
	        <p><a href="#footerAbout.getURL()#" class="btn btn-default" title="#footerAbout.getTitle()#">Learn More</a></p>
				</section>
				<!--- end: about us --->

				<!--- start: contact info --->
	      <section class="col-sm-4">
	        <h1>Contact Us</h1>
	        <nav>
						<ul class="fa-ul">
						  <li><i class="fa-li fa fa-location-arrow"></i> <a href="https://www.google.com/maps/dir/1534+S+1100+E,+Salt Lake City,+UT+84105">1534 S 1100 E<br>  Salt Lake City, UT 84105</a></li>
						  <li><i class="fa-li fa fa-phone-square"></i> <a href="tel:801-533-8530">(801) 533-8530</a></li>
						  <li><i class="fa-li fa fa-envelope"></i> <a href="mailto:info@jefflandrydesign.com" title="Jeff Landry Design Email Address">info@jefflandrydesign.com</a></li>
						</ul>
	        </nav>
	      </section>
				<!--- end: contact info --->

			</div>

	    <hr>

	    <div class="row">
	      <div class="col-sm-6">&copy;#year(now())# <b>#HTMLEditFormat($.siteConfig('site'))#</b>. All Rights Reserved.</div>
	      <div class="col-sm-6">Site by <a href="http://www.deven.io/" title="Devenio" rel="developer">Devenio</a>. Powered By <a href="http://www.getmura.com/" title="Get Mura" rel="CMS">Mura</a>.</div>
	    </div>

		</section>
		<!--- end: primary footer --->

	</footer>
	<!--- end: footer --->

	<cfif $.getcontentID() eq "00000000000000000000000000000000001"></div></cfif>

	<a id="toTop" class="btn"><i class="fa fa-chevron-up"></i></a> <!--- back to top link --->

</cfoutput>