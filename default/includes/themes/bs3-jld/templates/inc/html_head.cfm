<cfoutput>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="tr-coretext tr-aa-subpixel <cfif $.hasFETools()>mura-edit-mode </cfif>no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="tr-coretext tr-aa-subpixel <cfif $.hasFETools()>mura-edit-mode </cfif>no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="tr-coretext tr-aa-subpixel <cfif $.hasFETools()>mura-edit-mode </cfif>no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="tr-coretext tr-aa-subpixel <cfif $.hasFETools()>mura-edit-mode </cfif>no-js"> <!--<![endif]-->

	<head>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-58475934-1', 'auto');
		  ga('send', 'pageview');

		</script>

		<!--- META
		========== --->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>#HTMLEditFormat($.content('HTMLTitle'))# - #HTMLEditFormat($.siteConfig('site'))#</title>
		<meta name="description" content="#HTMLEditFormat($.getMetaDesc())#">
		<meta name="keywords" content="#HTMLEditFormat($.getMetaKeywords())#">
		<cfif request.contentBean.getCredits() neq ""><meta name="author" content="#HTMLEditFormat($.content('credits'))#" /></cfif>
		<meta name="generator" content="Mura CMS #$.globalConfig('version')#">

		<!--- Social meta --->
		<meta property="og:site_name" content="#HTMLEditFormat($.siteConfig('site'))#"/>
		<meta property="og:title" content="#HTMLEditFormat($.content('HTMLTitle'))#"/>
		<meta property="og:description" content="#HTMLEditFormat($.getMetaDesc())#"/>
		<cfif len($.getImageURL())><meta property="og:image" content="#$.getImageURL()#"></cfif>

		<!--- CSS - Theme styles - includes fonts and theme css       ==================================================================================== --->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic|Raleway:300,400,700,800" type="text/css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="#$.siteConfig('themeAssetPath')#/css/theme.min.css?v=1">

		<!--[if IE]>
			<link rel="stylesheet" href="#$.siteConfig('themeAssetPath')#/css/modules/ie.min.css">
		<![endif]-->

		<!--- FAV AND TOUCH ICONS
		========================= --->
		<link rel="shortcut icon" href="#$.siteConfig('themeAssetPath')#/images/ico/favicon.ico">
		<link rel="apple-touch-icon" sizes="57x57" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="114x114" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="144x144" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="60x60" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="120x120" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="76x76" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="152x152" href="#$.siteConfig('themeAssetPath')#/images/ico/apple-touch-icon-152x152.png">
		<link rel="icon" type="image/png" href="#$.siteConfig('themeAssetPath')#/images/ico/favicon-196x196.png" sizes="196x196">
		<link rel="icon" type="image/png" href="#$.siteConfig('themeAssetPath')#/images/ico/favicon-160x160.png" sizes="160x160">
		<link rel="icon" type="image/png" href="#$.siteConfig('themeAssetPath')#/images/ico/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="#$.siteConfig('themeAssetPath')#/images/ico/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="#$.siteConfig('themeAssetPath')#/images/ico/favicon-32x32.png" sizes="32x32">
		<meta name="msapplication-TileColor" content="##ffffff">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">

		<!--- MURA FEEDS --->
		<cfset rs=$.getBean('feedManager').getFeeds($.event('siteID'),'Local',true,true) />
		<cfloop query="rs"><link rel="alternate" type="application/rss+xml" title="#HTMLEditFormat($.siteConfig('site'))# - #HTMLEditFormat(rs.name)#" href="#XMLFormat('http://#listFirst(cgi.http_host,":")##$.globalConfig('context')#/tasks/feed/?feedID=#rs.feedID#')#"></cfloop>

		<!--- JQUERY - really wish these were in the footer!
		==================================================== --->
    <script src="#$.siteConfig('themeAssetPath')#/js/vendor/modernizr.custom.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="#$.siteConfig('themeAssetPath')#/js/vendor/jquery.1.11.1.min.js"><\/script>')</script>

	</head>
</cfoutput>