<cfoutput>
	<cfinclude template="inc/html_head.cfm">
	<cfinclude template="inc/header.cfm">
	<div class="container">
		<div class="row">
			<section class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main">
				<header><h1>#$.content('title')#</h1></header>
				<div class="lead">#$.content('summary')#</div>
				#$.dspBody(body=$.content('body'),pageTitle='',crumbList=0,showMetaImage=0,metaImageClass='thumbnail')#
				#$.dspObjects(2)#
			</section>
		</div>
	</div><!-- /.container -->
	<cfinclude template="inc/footer.cfm" />
	<cfinclude template="inc/html_foot.cfm" />
</cfoutput>