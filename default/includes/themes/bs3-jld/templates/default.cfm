<cfoutput>
  <cfinclude template="inc/html_head.cfm">
  <cfinclude template="inc/header.cfm">

  <!--- start: main content --->
  <section role="main">


    <!--- start: header --->
    <header class="page-header">
      <div class="container">
        <h1 class="page-title pull-left">#$.content('title')#</h1>
        <nav class="breadcrumb pull-right">#$.dspCrumbListLinks()#</nav>
      </div>
    </header>
    <!--- end: header --->

    <!--- start: switches: decide which template to serve up --->
    <cfscript>

      // start: main switch - change the list item output based on the type
      switch ( $.content('type') ) {

        // start: folder switch
        case 'Folder' :

         switch ( $.content('subType') ) {

          case 'Syndicated' :
            include '../display_objects/folders/dsp_folder_syndicated.cfm';
            break;

          default  :
            include '../display_objects/folders/dsp_folder_default.cfm';
        }
        break;
        // end: folder switch

        case 'Gallery' :
          include '../display_objects/pages/dsp_page_gallery.cfm';
          break;

        // start: non-folder switch
        case 'File' :
        case 'Link' :
        case 'Calendar' :
        case 'Page' :

          // switch the list item output based on the subType
          switch ( $.content('subType') ) {

            case 'Organization' :
              include '../display_objects/pages/dsp_page_organization.cfm';
              break;

            case 'Project' :
              include '../display_objects/pages/dsp_page_project.cfm';
              break;

            default :
              include '../display_objects/pages/dsp_page_default.cfm';
          }
        break;
        // end: non-folder switch

      };

    </cfscript>
    <!--- end: decide which template to serve up --->

  </section>
  <!--- end: main section --->

  <cfinclude template="inc/footer.cfm" />
  <cfinclude template="inc/html_foot.cfm" />
</cfoutput>