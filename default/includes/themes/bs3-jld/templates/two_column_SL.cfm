<cfoutput>
  <cfinclude template="inc/html_head.cfm">
  <cfinclude template="inc/header.cfm">

  <!--- start: main content --->
  <section role="main">

    <!--- start: slider --->
    <cfinclude template="../display_objects/content-modules/dsp_cm_fullslider.cfm">
    <!--- end: slider --->

    <div class="bg-b-left"></div>

    <!--- start: body content --->
    <div class="container">
      <div class="row">
        <nav class="col-sm-3">
          #$.dspObjects(1)#
        </nav>

        <section class="col-sm-8 col-sm-offset-1">
          <header><h1>#$.content('title')#</h1></header>
          <div class="lead">#$.content('summary')#</div>
          #$.dspBody(body=$.content('body'),pageTitle='',crumbList=0,showMetaImage=0)#
        </section>
      </div>
    </div>
    <!--- end: body content --->

  <!---
    <!--- start: featured content --->
    <cfinclude template="../display_objects/content-modules/dsp_cm_featured.cfm">
    <!--- end: featured content ---> --->

    <!--- start: resources --->
    <cfinclude template="../display_objects/content-modules/dsp_cm_resources.cfm">
    <!--- end: resources --->

  </section>
  <!--- end: main section --->

  <cfinclude template="inc/footer.cfm" />
  <cfinclude template="inc/html_foot.cfm" />
</cfoutput>