<!---
	This file is part of Mura CMS.

	Mura CMS is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, Version 2 of the License.

	Mura CMS is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See theƒ
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mura CMS. If not, see <http://www.gnu.org/licenses/>.

	Linking Mura CMS statically or dynamically with other modules constitutes
	the preparation of a derivative work based on Mura CMS. Thus, the terms
	and conditions of the GNU General Public License version 2 ("GPL") cover
	the entire combined work.

	However, as a special exception, the copyright holders of Mura CMS grant
	you permission to combine Mura CMS with programs or libraries that are
	released under the GNU Lesser General Public License version 2.1.

	In addition, as a special exception, the copyright holders of Mura CMS
	grant you permission to combine Mura CMS with independent software modules
	(plugins, themes and bundles), and to distribute these plugins, themes and
	bundles without Mura CMS under the license of your choice, provided that
	you follow these specific guidelines:

	Your custom code

	• Must not alter any default objects in the Mura CMS database and
	• May not alter the default display of the Mura CMS logo within Mura CMS and
	• Must not alter any files in the following directories:

		/admin/
		/tasks/
		/config/
		/requirements/mura/
		/Application.cfc
		/index.cfm
		/MuraProxy.cfc

	You may copy and distribute Mura CMS with a plug-in, theme or bundle that
	meets the above guidelines as a combined work under the terms of GPL for
	Mura CMS, provided that you include the source code of that other code when
	and as the GNU GPL requires distribution of source code.

	For clarity, if you create a modified version of Mura CMS, you are not
	obligated to grant this special exception for your modified version; it is
	your choice whether to do so, or to make such modified version available
	under the GNU General Public License version 2 without this exception.  You
	may, if you choose, apply this exception to your own modified versions of
	Mura CMS.
--->
<cfsilent>
	<cfif not structKeyExists(arguments,"type")>
		<cfset arguments.type="Feed">
	</cfif>

	<cfif not structKeyExists(arguments,"fields")>
		<cfset arguments.fields="Date,Title,Image,Summary,Credits,Tags">
	</cfif>

	<cfset arguments.hasImages=listFindNoCase(arguments.fields,"Image")>

	<cfif arguments.hasImages>
		<cfset arguments.isCustomImage= false />

		<cfif not structKeyExists(arguments,"imageSize") or variables.$.event("muraMobileTemplate")>
			<cfset arguments.imageSize="small">
		<cfelseif not listFindNoCase('small,medium,large,custom',arguments.imagesize)>
			<cfset arguments.customImageSize=getBean('imageSize').loadBy(name=arguments.imageSize,siteID=variables.$.event('siteID'))>
			<cfset arguments.imageWidth= arguments.customImageSize.getWidth() />
			<cfset arguments.imageHeight= arguments.customImageSize.getHeight() />
			<cfset arguments.isCustomImage= true />
		</cfif>

		<cfif not structKeyExists(arguments,"imageHeight")>
			<cfset arguments.imageHeight="auto">
		</cfif>

		<cfif not structKeyExists(arguments,"imageWidth")>
			<cfset arguments.imageWidth="auto">
		</cfif>

		<cfif not structKeyExists(arguments,"imagePadding")>
			<cfset arguments.imagePadding=20>
		</cfif>

		<cfif this.contentListImageStyles>
			<cfif arguments.isCustomImage>
				<cfset arguments.imageStyles='style="#variables.$.generateListImageSyles(size='custom',width=arguments.imageWidth,height=arguments.imageHeight,padding=arguments.imagePadding)#"'>
			<cfelse>
				<cfset arguments.imageStyles='style="#variables.$.generateListImageSyles(size=arguments.imageSize,width=arguments.imageWidth,height=arguments.imageHeight,padding=arguments.imagePadding)#"'>
			</cfif>
		</cfif>
	</cfif>

  <!--- start: grid option --->
  <cfif arguments.iterator.getRecordCount() mod 2>
    <cfset local.templateClass = "col-sm-4">
  <cfelse>
    <cfset local.templateClass = "col-sm-6" />
  </cfif>
  <!--- end: grid option --->

</cfsilent>

<cfoutput>

<!--- start: iterator --->
#$.dspRenderTemplate(
	template                        = 'default'
	, content                       = arguments.iterator
	, nextN                         = $.content('nextN')
	, fieldlist                     = arguments.fields
	, collectionWrapper             = 'section'
	, collectionWrapperID           = ''
	, collectionWrapperClass        = 'bg-g'
	, collectionContainer           = 'div'
	, collectionContainerID         = ''
	, collectionContainerClass      = 'container'
	, collectionInnerContainer      = 'div'
	, collectionInnerContainerID    = ''
	, collectionInnerContainerClass = 'row'
	, heading                       = ''
	, headingDesc                   = ''
	, templateWrapper               = 'article'
  , templateClass                 = local.templateClass
	, imageOrientation              = 'top'
	, imageClass                    = ''
	, imageSize                     = 'medium'
)#
<!--- end: iterator --->

</cfoutput>