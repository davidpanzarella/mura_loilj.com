<!--- Get Featured and Slideshow content for homepage --->
<cfsilent>
  <cfset local.itProjects = $.content().getRelatedContentIterator(name='Related Projects')>
  <cfset local.itServices = $.content().getRelatedContentIterator(name='Related Services')>
</cfsilent>

<cfoutput>

  <!--- start: slider --->
  <cfinclude template="../content-modules/dsp_cm_fullslider.cfm">
  <!--- end: slider --->

  <!--- start: body content --->
  <div class="container">
    <header><h1>#$.content('title')#</h1></header>
    <div class="lead">#$.content('summary')#</div>
    #$.dspBody(body=$.content('body'),pageTitle='',crumbList=0,showMetaImage=0,metaImageClass='img-responsive')#
  </div>
  <!--- end: body content --->

  <cfdump var="#local.itServices#" />

  <!--- start: featured services --->
  <cfif local.itServices.hasNext()>

      <!--- start: iterator --->
      #$.renderCollection.getLayoutTemplate(
        iterator                             = local.itServices
        , collectionWrapper                  = 'section'
        , collectionWrapperAttributes        = ''
        , collectionContainer                = ''
        , collectionContainerAttributes      = ''
        , heading                            = ''
        , headingDesc                        = ''
        , collectionInnerContainer           = ''
        , collectionInnerContainerAttributes = ''
        , template                           = 'default'
        , templateWrapper                    = 'article'
        , imageAttributes                    = ''
        , imageSize                          = 'small'
      )#
      <!--- end: iterator --->

  </cfif>
  <!--- end: featured services --->

  <!--- start: featured content --->
  <!--- <cfinclude template="../content-modules/dsp_cm_accomodations_slider.cfm"> --->
  <!--- end: featured content --->
<!---
  <section class="bg-b tpl-feature">
    <div class="container">
      <header><h1>#local.diningBean.getTitle()#</h1></header>
      <div class="row text-left">
        <div class="col-md-6">
          <figure><img src="#diningBean.getImageURL(size='medium')#" alt="#local.diningBean.getTitle()#"></figure>
        </div>
        <div class="col-md-6">
          #$.dspComponent('Dining Component')#
          <!--- #local.diningBean.getSummary()#<hr>
          #local.diningBean.getBody()#
          <p>
            <a href="http://www.root-246.com" class="btn btn-primary btn-lg">Visit Root 246</a>
            <a href="/dining/" class="btn btn-default btn-lg">More Dining</a>
          </p> --->
        </div>
      </div>
    </div>
  </section> --->

</cfoutput>