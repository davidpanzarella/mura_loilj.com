<cfsilent>
  <cfset local.itGallerySlider = $.content().getRelatedContentIterator(name='Related Images')>
  <cfset local.itCategories = $.content().getCategoriesIterator()>
</cfsilent>

<cfoutput>

  <!--- start: body content --->
  <nav class="container">
    <div class="row">
      #$.dspPrevNextNav()#
      <div class="col-md-6 text-right">
				<div class="addthis_sharing_toolbox"></div>
      </div>
    </div>
	</nav>

  <section class="container">
    <cfif len($.getImageURL())>
      <figure><img src="#$.getImageURL(size='large')#" alt="#$.getMenuTitle()#" class="img-responsive mbe fancybox-thumbs" data-fancybox-group="thumb"></figure>
    </cfif>

		<div class="row">
			<div class="col-sm-8">
		    <cfif len($.content('summary'))><div class="lead">#$.content('summary')#</div></cfif>
		    #$.dspBody(body=$.content('body'),pageTitle='',crumbList=0,showMetaImage=0,metaImageClass='img-responsive')#
			</div>

			<div class="col-sm-4">
				<dl>

					<cfif len($.content('projectLocation'))>
	          <dt>Location</dt>
	          <dd>#$.content('projectLocation')#</dd>
	        </cfif>

					<cfif local.itCategories.hasNext()>
	          <dt>Style</dt>
              <cfloop condition="local.itCategories.hasNext()">
	              <cfset local.category = local.itCategories.next()>
	              <cfif local.category.getParent().getCategoryID() eq "D22F49CF-8598-483D-A1669B6C527AF7FD">
	                <dd>#HTMLEditFormat(local.category.getName())#</dd>
	              </cfif>
	            </cfloop>
	            <cfset local.itCategories.reset() />
            </dt>
          </cfif>

          <cfif local.itCategories.hasNext()>
	          <dt>Room Types</dt>
              <cfloop condition="local.itCategories.hasNext()">
	              <cfset local.category = local.itCategories.next()>
	              <cfif local.category.getParent().getCategoryID() eq "AC23684E-AACF-4803-A26738E7AE49B393">
	                <dd>#HTMLEditFormat(local.category.getName())#</dd>
	              </cfif>
	            </cfloop>
            </dt>
          </cfif>

        </dl>
			</div>
		</div>
	</section>

  <!--- start: iterator --->
  <cfif local.itGallerySlider.hasNext()>
    <section class="content-area">
      <div class="bg-img" style="background: url(#$.getImageUrl('carouselimage')#);"></div>

	    <div class="container" id="miniSlider">

        <header><h1 class="text-center">#$.content('title')# Project Gallery</h1></header>

        <!--- start: carousel --->
        <div class="flexslider carousel">
          <ul class="slides">

            <cfloop condition="local.itGallerySlider.hasNext()">
              <cfset r = local.itGallerySlider.next()>
              <cfif r.getDisplay() eq 1>
	              <li>
									<a href="#r.getImageURl(size='large')#" title="#r.getTitle()#"  class=" fancybox-thumbs" data-fancybox-group="thumb" rel="gallery">
									  <img src="#r.getImageURL(size="carouselimage")#" class="img-responsive"  alt="#r.getTitle()#" />
									</a>
	              </li>
							</cfif>
            </cfloop>

          </ul>
        </div>
        <!--- end: carousel --->

        <script type="text/javascript">
          (function() {

            // store the slider in a local variable
            var $window = $(window),
                flexslider;

            // tiny helper function to add breakpoints
            function getGridSize() {
              return (window.innerWidth < 481) ? 1 :
                     (window.innerWidth < 767) ? 2 :
                     (window.innerWidth < 1139) ? 3 : 4;
            }

            $window.load(function() {
              $('##miniSlider .flexslider').flexslider({
                animation: "slide",
                directionNav: true,
                controlNav: false,
                animationSpeed: 400,
                animationLoop: true,
                itemWidth: 295,
                itemMargin: 10,
                minItems: getGridSize(), // use function to pull in initial value
                maxItems: getGridSize(), // use function to pull in initial value
                start: function(slider){
                  $('body').removeClass('loading');
                  flexslider = slider;
                }
              });
            });

            // check grid size on resize event
            $window.resize(function() {
              var gridSize = getGridSize();

              flexslider.vars.minItems = gridSize;
              flexslider.vars.maxItems = gridSize;
            });
          }());

        </script>

	    </div>

		</section>
  </cfif>
  <!--- end: iterator --->

  <!--- end: body content --->
</cfoutput>