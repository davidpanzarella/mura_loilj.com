<cfoutput>
  <!--- start: body content --->
  <section class="container">
    <cfif len($.content('summary'))><div class="lead">#$.content('summary')#</div></cfif>

    <!--- start: Gallery Items --->
    <cfif $.getKidsIterator().hasNext()>

      <!--- start: iterator --->
		  #$.renderCollection.dspRenderCollection(
		    data                                 = ''
		    , type                               = ''
		    , subType                            = ''
        , parentID                           = $.content().getContentID()
		    , maxItems                           = 0
		    , sortby                             = $.content('sortBy')
		    , sortDirection                      = $.content('sortDirection')

		    , filterFields                       = 'Room Types,Styles'
		    , filterRelatedFields                = ''
		    , filters                            = "filter=#$.event('filter')#"

		    , nextN                              = $.content('nextN')
		    , startRow                           = $.event('startRow')

		    , order                              = "filter,template,pagination"

		    , filterView                         = true
		    , filterMappedCategory               = 'Room Types,Styles'
		    , filterDisplay                      = 'link'
		    , filterType                         = 'multiple'
		    , filterGroup                        = 'single'
		    , filterCounts                       = false

		    , filterWrapper                      = 'div'
		    , filterWrapperAttributes            = ''
		    , filterShowMessage     						 = false
		    , filterGroupWrapperAttributes       = 'class="filter-nav"'

        , collectionWrapper                  = 'section'
        , collectionWrapperAttributes        = 'class="content-area text-center"'
        , collectionContainer                = 'div'
        , collectionContainerAttributes      = ''
        , collectionInnerContainer           = 'div'
        , collectionInnerContainerAttributes = 'id="galleryContainer" class="clearfix col-4"'
        , template                           = 'gallery'
        , fieldlist                          = ''
        , templateWrapper                    = 'div'
        , templateWrapperAttributes          = 'class="galleryItem animated activate fadeInUp" data-fx="fadeInUp"'
        , imageSize                          = 'small'
      )#
      <!--- end: iterator --->

    </cfif>
    <!--- end: Gallery Items --->

  </section>
  <!--- end: body content --->
</cfoutput>