<cfsilent>
    <!--- get the content bean from the Jeff's bio page --->
  <cfset local.jeff = $.getBean("content").loadBy(contentID="BB0D820B-BAA5-4698-9DB88403996BFEF9",siteID=$.event("siteid"))>
</cfsilent>

<cfoutput>

  <!--- start: body content --->
  <div class="container">
    <cfif len($.getImageURL())>
      <figure class="pull-left"><img src="#$.getImageURL(size='medium')#" alt="#$.getMenuTitle()#"></figure>
    </cfif>

    <cfif len($.content('contentTagLine'))><h2>#$.content('contentTagLine')#</h2></cfif>
    <div class="lead">#$.content('summary')#</div>
    #$.dspBody(body=$.content('body'),pageTitle='',crumbList=0,showMetaImage=0,metaImageClass='img-responsive')#
		#$.dspObjects(2)#
  </div>
  <!--- end: body content --->

  <!--- start: about us section --->
  <cfif $.content('contentid') eq "BE5EE374-659D-45AD-B4A7E3B9631F4D6C">

    <!--- start: all services --->
    #$.renderCollection.dspRenderCollection(
      data                                 = ''
      , type                               = 'Page'
      , subType                            = 'Service'
      , maxItems                           = 0
      , order                              = "template"

      , collectionWrapper                  = 'section'
      , collectionWrapperAttributes        = 'class="content-area bg-b text-center"'
      , collectionContainer                = 'div'
      , collectionContainerAttributes      = 'class="container"'
      , heading                            = '<header><h1>Our Services Include</h1></header>'
      , headingDesc                        = ''
      , collectionInnerContainer           = 'div'
      , collectionInnerContainerAttributes = 'class="row" role="service-collection"'
      , template                           = 'service'
      , fieldlist                          = ''
      , templateWrapper                    = 'article'
      , templateWrapperAttributes          = 'class="col-sm-6 col-md-4 animated activate fadeInUp" data-fx="fadeInUp"'
      , imageOrientation                   = 'top'
      , imageAttributes                    = ''
      , imageSize                          = ''

    )#
    <!--- end: all services --->


    <section class="container">
      <figure class="pull-left">
        <img class="img-responsive" src="#local.jeff.getImageURL(size="portrait")#" alt="#local.jeff.getValue('title')#" title="#local.jeff.getValue('title')#" />
      </figure>
      <h2>#local.jeff.getValue('contentTagLine')#</h2>
      <div class="lead">#local.jeff.getValue('summary')#</div>
      #local.jeff.getValue('body')#
    </section>
  </cfif>

</cfoutput>