<cfsilent>
	<cfset local.itKidList = $.getKidsIterator()>
</cfsilent>
<cfoutput>

  <!--- start: slider --->
  <cfinclude template="../content-modules/dsp_cm_fullslider.cfm">
  <!--- end: slider --->

  <!--- start: body content --->
  <div class="container">
    <header><h1>#$.content('title')#</h1></header>
    <div class="lead">#$.content('summary')#</div>
  </div>

	<section class="tpl-feature">
		<div class="bg-img" style="background: url(#$.getImageUrl('carouselimage')#);"></div>
	  <div class="container">
      <header><h1>#$.dspTagStripper($.content('contentFeaturesTitle'),'strip','strong')#</h1></header>
	    <div class="row">
	      <div class="col-md-6">
	        <figure class="mbl"><img src="#$.getImageURL(size='medium')#" class="img-responsive" alt="#$.getTitle()#"></figure>
	      </div>
	      <div class="col-md-6">#$.getBody()#</div>
	    </div>
	  </div>
	</section>
  <!--- end: body content --->

	<!--- start: kids iterator  --->
	<cfif local.itKidList.hasNext()>

		<!--- start: loop over kids --->
		<!--- we want to loop over the folders within this folder, and take the kidsIterators from those folder, and pass them into our dspRenderTemplate(), so that we can get the right layout --->


		<!--- end: loop over kids --->
		<cfloop condition="local.itKidList.hasNext()">
			<cfset kid = local.itKidList.next()>

			<!--- start: heading & desc --->
		  <cfset local.featuresTitle = '<header><h1>' & kid.getTitle() & '</h1></header>' />
		  <cfif len(kid.getSummary())>
		    <cfset local.featuresTitleDesc = '<div class="lead">' &  kid.getSummary() & '</div>' />
		  <cfelse>
		    <cfset local.featuresTitleDesc = "">
		  </cfif>
			<!--- end: heading & desc --->

			<!--- start: background --->
			<cfif local.itKidList.currentIndex() mod 2>
		    <cfset local.wrapperClass = "bg-g" />
		    <cfset local.templateClass = "col-sm-6" />
		  <cfelse>
		    <cfset local.wrapperClass = "bg-w">
		    <cfset local.templateClass = "col-sm-4">
		  </cfif>
			<!--- end: background --->

	    <!--- start: iterator --->
	    #$.dspRenderTemplate(
				template                        = 'default'
				, content                       = kid.getKidsIterator()
				, nextN                         = 20
				, fieldlist                     = ''
				, collectionWrapper             = 'section'
				, collectionWrapperID           = ''
				, collectionWrapperClass        = local.wrapperClass
				, collectionContainer           = 'div'
				, collectionContainerID         = ''
				, collectionContainerClass      = 'container'
				, collectionInnerContainer      = 'div'
				, collectionInnerContainerID    = ''
				, collectionInnerContainerClass = 'row'
				, heading                       = local.featuresTitle
				, headingDesc                   = local.featuresTitleDesc
				, templateWrapper               = 'article'
				, templateClass                 = local.templateClass
				, imageOrientation              = 'top'
				, imageClass                    = ''
				, imageSize                     = 'medium'
	    )#
	    <!--- end: iterator --->


		</cfloop>
	</cfif>
	<!--- end: kids iterator --->

</cfoutput>