<!--- default layout --->

<cfsilent>
  <!--- start: heading & desc --->
  <cfset local.folderTagline = '<h2 class="text-center">' & $.content('contentTagline') & '</h2>' />
  <cfif len($.content('summary'))>
    <cfset local.folderDesc = '<div class="lead text-center">' & $.content('summary') & '</div><hr>' />
  <cfelse>
    <cfset local.folderDesc = "">
  </cfif>
  <!--- end: heading & desc --->
</cfsilent>

<cfoutput>

  <!--- start: folder Items --->
  <cfif $.getKidsIterator().hasNext()>

    <!--- start: iterator --->
    #$.renderCollection.dspRenderCollection(
      data                                 = ''
      , type                               = ''
      , subType                            = ''
      , parentID                           = $.content().getContentID()
      , maxItems                           = 0
      , sortby                             = $.content('sortBy')
      , sortDirection                      = $.content('sortDirection')
      , nextN                              = $.content('nextN')
      , startRow                           = $.event('startRow')

      , order                              = "template,pagination"

      , collectionWrapper                  = 'section'
      , collectionWrapperAttributes        = 'class="content-area container"'
      , collectionContainer                = ''
      , collectionContainerAttributes      = ''
      , collectionInnerContainer           = 'div'
      , collectionInnerContainerAttributes = 'class="row" role="project-collection"'

      , heading                            = local.folderTagline
      , headingDesc                        = local.folderDesc

      , template                           = 'default'
      , fieldlist                          = ''
      , templateWrapper                    = 'article'
      , templateWrapperAttributes          = 'class="col-sm-6 col-md-4 animated activate fadeInUp" data-fx="fadeInUp"'
      , imageOrientation                   = 'top'
      , imageAttributes                    = ''
      , imageSize                          = ''
      , imageSize                          = 'small'
    )#
    <!--- end: iterator --->

  </cfif>
  <!--- end: folder Items --->

</cfoutput>