<!--- Folder - Organizations Layout --->
<cfoutput>

<cfinclude template="../content-headers/dsp_header_default.cfm">

<!--- start: kids iterator --->
<cfset kidList   = $.getBean("content").loadBy(contentID = $.content('contentID'), siteId = $.event("siteid"))>
<cfset itKidList = kidList.getKidsIterator()>
<cfif itKidList.hasNext()>
	<div class="row" role="organization-collection">
	<cfloop condition="itKidList.hasNext()">
		<cfset o = itKidList.next()>

		<!--- check to see if CWB or CWC is set as an associated organization --->
		<cfset poIt = o.getRelatedContentIterator(name='Associated Organizations')>

		<article class="col-sm-6 pull-height">
			<header>
				<h1><a href="#o.getURL()#" title="Learn more about #o.getMenuTitle()#">#o.getMenuTitle()#</a></h1>
				<cfif poIt.hasNext()>
					<cfloop condition="poIt.hasNext()">
						<cfset po = poIt.next()>
						<cfif listContainsNoCase("C95BDC18-6A43-437D-929967A417DE4E93,EFA60244-E405-462F-B39A60EE6EDED081", po.getContentID())>
							<cfif poIt.getRecordIndex() eq 1><h2 class="h4">Administered by:</cfif>
						 		<a href="#po.getURL()#" title="Learn more about #po.getMenuTitle()#">#po.getMenuTitle()#</a>
						 		<cfif poIt.getRecordCount() neq poIt.getRecordIndex()> &amp;<cfelse></h2></cfif>
						</cfif>
					</cfloop>
				</cfif>
			</header>
			#$.setDynamicContent(o.getValue('summary'))#
		</article>
	</cfloop>
	</div>
</cfif>
<!--- end: kids iterator --->

<cfinclude template="../content-footers/dsp_footer_default.cfm">
</cfoutput>