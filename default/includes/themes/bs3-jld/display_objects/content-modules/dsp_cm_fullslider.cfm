<cfsilent>
  <cfset local.itSlideshow = $.content().getRelatedContentIterator(name='Slideshow Content')>
</cfsilent>

<cfoutput>

  <!--- start: content --->
  <cfif local.itSlideshow.hasNext()>

    <div class="parallaxSection height100">

      <!--- start: iterator --->
      #$.renderCollection.getLayoutTemplate(
        iterator                             = local.itSlideshow
        , collectionWrapper                  = 'section'
        , collectionWrapperAttributes        = 'class="flexslider std-slider" data-height="560" data-loop="true" data-smooth="false" data-slideshow="true" data-speed="6000" data-animspeed="550" data-controls="true" data-dircontrols="true"'
        , collectionContainer                = ''
        , collectionContainerAttributes      = ''
        , heading                            = ''
        , headingDesc                        = ''
        , collectionInnerContainer           = 'ul'
        , collectionInnerContainerAttributes = 'class="slides"'
        , template                           = 'fullslider'
        , templateWrapper                    = 'li'
        , imageAttributes                    = ''
        , imageSize                          = 'sliderimage'
      )#
      <!--- end: iterator --->

      <script type="text/javascript">
        $(document).ready(function() {
          $('##parallaxSection .flexslider').flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            start: function(slider){
              $('body').removeClass('loading');
            }
          });
        });
      </script>

      <a href="##nextSection" class="bigArrow local menuOffset"><i class="fa fa-angle-down" style="opacity: 1; top: 0px;"></i></a>

    </div>

  </cfif>
  <!--- end: content --->

</cfoutput>

<!---
  <!--- start: iterator --->
  <cfif local.itSlideshow.hasNext()>
    <section id="fullSlider">

      <!--- start: carousel --->
      <div class="flexslider">
        <ul class="slides">
          <cfloop condition="local.itSlideshow.hasNext()">
            <cfset r = local.itSlideshow.next()>
            <li style="background: url(#r.getImageUrl('carouselimage')#);">
              <a href="#r.getUrl()#" title="#r.getTitle()#">
                <!--- <img src="#r.getImageUrl('carouselimage')#" alt="#r.getTitle()#"> --->
                <div class="flex-caption">#r.getTitle()#</div>
              </a>
            </li>
          </cfloop>

        </ul>
      </div>
      <!--- end: carousel --->



    </section>
  </cfif>
  <!--- end: iterator --->

</cfoutput> --->