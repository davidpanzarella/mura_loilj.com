<cfsilent>
  <!--- <cfset local.itProjects = gallery.getRelatedContentIterator(name='Related Projects')> --->
  <cfset local.itCategories = gallery.getCategoriesIterator()>
</cfsilent>
<cfoutput>
  <#arguments.templateWrapper# <cfif len(arguments.templateWrapperAttributes)>#arguments.templateWrapperAttributes# #arguments.templateWrapperAttributes# </cfif>>

    <a href="#gallery.getImageURl(size='large')#" class="area-hover fancybox-thumbs" data-fancybox-group="thumb" title="#gallery.getTitle()#">
      <div class="vertical-parent">
        <div class="vertical-child">

          <cfif local.itCategories.hasNext()>
            <span class="cat-links">
              <cfloop condition="local.itCategories.hasNext()">
                <cfset local.category = local.itCategories.next()>
                #HTMLEditFormat(local.category.getName())#
              </cfloop>
            </span>
          </cfif>

          <!--- <cfif local.itProjects.hasNext()>
            <cfset local.project = local.itProjects.next()>
            <span class="cat-links"><a href="#local.project.getURL()#" title="Learn more about #local.project.getTitle()#">#local.project.getTitle()#<!--- </a> ---></span>
          </cfif> --->
          <h4 class="entry-title">#gallery.getTitle()#</h4>
        </div>
      </div>
    </a>

    <a href="#gallery.getImageURl(size='large')#" title="#gallery.getTitle()#" rel="gallery">
      <img src="#gallery.getImageURL(size=arguments.imageSize)#" class="img-responsive" />
    </a>
  </#arguments.templateWrapper#>
</cfoutput>