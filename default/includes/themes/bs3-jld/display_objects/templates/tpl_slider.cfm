<cfoutput>
  <!--- <cfdump var="#slider.getAllValues()#" abort /> --->
  <#arguments.templateWrapper# style="background: url(#slider.getImageUrl('large')#) 50% 50%;">
    <div class="flex-caption">
      <h1><a href="#slider.getUrl()#" title="#slider.getTitle()#">#slider.getMenuTitle()#</a></h1>
      <div class="hidden-xs">#slider.getSummary()#</div>
      <cfif len(slider.getFacFeature1()) AND len(slider.getFacFeature2()) AND len(slider.getFacFeature3())>
        <hr>
        <div class="row">
          <div class="col-sm-4">#slider.getFacFeature1()#</div>
          <div class="col-sm-4">#slider.getFacFeature2()#</div>
          <div class="col-sm-4">#slider.getFacFeature3()#</div>
        </div>
      </cfif>
    </div>
  </#arguments.templateWrapper#>
</cfoutput>