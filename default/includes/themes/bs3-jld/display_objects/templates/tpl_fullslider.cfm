<cfif len(fullslider.getSlideImage())>
  <cfset local.image = $.getURLForImage(fileid=fullslider.getSlideImage(),size='sliderimage') />
<cfelse>
  <cfset local.image = fullslider.getImageUrl('sliderimage') />
</cfif>

<cfoutput>
  <#arguments.templateWrapper# data-bg="#local.image#" <cfif fullslider.getSlideLinkable()>data-href="#fullslider.getUrl()#" class="link"</cfif>>
    <div class="container">
      <div class="inner">
        <div class="text-center">
          <h1 class="page-title animated" data-fx="fadeInDown">#fullslider.getSlideTitle()#</h1>

          <cfif len(fullslider.getSlideDesc())>
            <h2 class="animated" data-fx="fadeIn">#fullslider.getSlideDesc()#</h2>
          </cfif>

          <!--- check to see if this slide should link --->
          <cfif fullslider.getSlideLinkable()>
            <a href="#fullslider.getUrl()#" data-fx="fadeInLeft" title="#fullslider.getSlideTitle()#" class="btn btn-default btn-lg animated">#fullslider.getSlideLinkLabel()#</a>
          </cfif>

        </div>
      </div>
    </div>
  </#arguments.templateWrapper#>
</cfoutput>