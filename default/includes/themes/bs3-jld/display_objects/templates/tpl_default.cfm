<cfsilent>
  <cfparam name="arguments.template" default="default"/>
  <cfparam name="arguments.templateWrapper" default="article"/>

  <cfif isDefined('arguments.item')>
    <cfparam name="arguments.default" default="#arguments.item#"/>
  </cfif>

  <cfif arguments.default.getType() eq "File">
    <cfset image = $.createHREFForImage(fileID=arguments.default.getFileImage(),size=arguments.imageSize) />
  <cfelse>
    <cfset image = arguments.default.getImageURL(size=arguments.imageSize) />
  </cfif>

  <cfparam name="arguments.imageOrientation" default="top"/>
  <cfparam name="arguments.imageClass" default="img-responsive"/>
  <cfparam name="arguments.imageSize" default="small"/>

  <cfset local.itCategories = arguments.default.getCategoriesIterator()>

</cfsilent>
<cfoutput>

  <!--- <cfdump var="#$.dspFullLeft("arguments.default.getValue('summary')", 100)#" > --->
  <!--- at this point, we know we have an object called default, so we can use the normal attributes within this loop --->
  <#arguments.templateWrapper# <cfif len(arguments.templateWrapperAttributes)>#arguments.templateWrapperAttributes# #arguments.templateWrapperAttributes# </cfif>>
    <div class="box">
      <cfif len(image)>
        <cfif len(arguments.default.getContentTagLine())>
          <span>#arguments.default.getContentTagLine()#</span>
        </cfif>
        <a href="#arguments.default.getURL()#" <cfif arguments.default.getType() eq "File" OR  arguments.default.getType() eq "Link"> class="external"</cfif>>
          <figure class="#arguments.imageOrientation#">
            <img class="#arguments.imageClass# img-responsive" src="#image#" alt="#arguments.default.getValue('title')#" title="#arguments.default.getValue('title')#" />
          </figure>
        </a>
      </cfif>
      <header>


        <h1><a href="#arguments.default.getURL()#" title="Learn more about #arguments.default.getValue('title')#" <cfif arguments.default.getType() eq "File" OR  arguments.default.getType() eq "Link"> class="external"</cfif>> #arguments.default.getValue('menuTitle')#</a></h1>
      </header>
      <div>
        <cfif local.itCategories.hasNext()>
          <span class="cat-links">A
            <cfloop condition="local.itCategories.hasNext()">
              <cfset local.category = local.itCategories.next()>
              <cfif local.category.getParent().getCategoryID() eq "D22F49CF-8598-483D-A1669B6C527AF7FD">
                #HTMLEditFormat(local.category.getName())#
              </cfif>
            </cfloop>
             Project
          </span>
        </cfif>
        <cfif len(arguments.default.getValue('summary'))>#arguments.default.getValue('summary')#</cfif>
        <!--- <p><a href="#arguments.default.getValue('url')#" title="Learn more about #arguments.default.getValue('title')#" class="btn btn-primary">Lern More</a></p> --->
      </div>
    </div>
  </#arguments.templateWrapper#>
</cfoutput>