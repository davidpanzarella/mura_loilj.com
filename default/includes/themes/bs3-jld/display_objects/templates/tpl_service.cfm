<cfoutput>
  <#arguments.templateWrapper# <cfif len(arguments.templateWrapperAttributes)>#arguments.templateWrapperAttributes# #arguments.templateWrapperAttributes# </cfif> role="service">
    <div class="box-icon">
      <cfif len(arguments.service.getServiceIcon())>
        <figure class="#arguments.imageOrientation#">
          <i class="fa #arguments.service.getServiceIcon()#"></i>
        </figure>
      </cfif>
      <header>
        <h1>
          <cfif !arguments.service.getContentLinkable()>
            #arguments.service.getValue('menuTitle')#
          <cfelse>
            <a href="#arguments.service.getURL()#" title="Learn more about #arguments.service.getValue('title')#" <cfif arguments.service.getType() eq "File" OR  arguments.service.getType() eq "Link"> class="external"</cfif>> #arguments.service.getValue('menuTitle')# </a>
          </cfif>
        </h1>
      </header>
      <cfif len(arguments.service.getValue('summary'))>#arguments.service.getValue('summary')#</cfif>
      <!--- <p><a href="#arguments.service.getValue('url')#" title="Learn more about #arguments.service.getValue('title')#" class="btn btn-primary">Lern More</a></p> --->
    </div>
  </#arguments.templateWrapper#>
</cfoutput>