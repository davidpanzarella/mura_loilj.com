<cfsilent>
  <cfparam name="arguments.template" list="list"/>
  <cfparam name="arguments.templateWrapper" list="article"/>

  <cfif isDefined('arguments.item')>
    <cfparam name="arguments.list" list="#arguments.item#"/>
  </cfif>

  <cfif arguments.list.getType() eq "File">
    <cfset image = $.createHREFForImage(fileID=arguments.list.getFileImage(),size=arguments.imageSize) />
  <cfelse>
    <cfset image = arguments.list.getImageURL(size=arguments.imageSize) />
  </cfif>

  <cfparam name="arguments.imageOrientation" list="top"/>
  <cfparam name="arguments.imageClass" list="img-responsive"/>
  <cfparam name="arguments.imageSize" list="small"/>

</cfsilent>
<cfoutput>
  <#arguments.templateWrapper# class="#arguments.templateClass# list">
    <h2>#arguments.list.getValue('title')#</h2>
    <cfif len(arguments.list.getValue('summary'))>#arguments.list.getValue('summary')#</cfif>
	</#arguments.templateWrapper#>
</cfoutput>