<cfsilent>
  <cfparam name="arguments.template" default="listsimplesimple"/>
  <cfparam name="arguments.templateWrapper" default="li"/>

  <cfif isDefined('arguments.item')>
    <cfparam name="arguments.listsimple" default="#arguments.item#"/>
  </cfif>

  <cfif arguments.listsimple.getType() eq "File">
    <cfset image = $.createHREFForImage(fileID=arguments.listsimple.getFileImage(),size=arguments.imageSize) />
  <cfelse>
    <cfset image = arguments.listsimple.getImageURL(size=arguments.imageSize) />
  </cfif>

  <cfparam name="arguments.imageOrientation" default="top"/>
  <cfparam name="arguments.imageClass" default="img-responsive"/>
  <cfparam name="arguments.imageSize" default="small"/>

</cfsilent>
<cfoutput>
  <#arguments.templateWrapper# class="#arguments.templateClass# listsimple">
    <strong>#arguments.listsimple.getValue('title')#</strong><br>
    <cfif len(arguments.listsimple.getValue('summary'))>#arguments.listsimple.getValue('summary')#</cfif>
	</#arguments.templateWrapper#>
</cfoutput>